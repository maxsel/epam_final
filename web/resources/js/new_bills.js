window.onload = function () {
    $('#errorMsg').hide();
}

function setBillIdAndConfirm(id) {
    $('[name=command]').val('confirmBill');
    $('[name=billIdToConfirm]').val(id);
    document.confirmForm.submit();
}
function setBillIdAndPay(id) {
    $('[name=command]').val('payBill');
    $('[name=billIdToPay]').val(id);
    
    //document.confirmForm.submit();
    var $form = $('#confirmForm');
    $.post($form.attr("action"), $form.serialize(), function (resp) {
        if (resp == "fail") {
            $('#errorMsg').show();
        } else {
            window.location.href = resp;
        }
    });
    event.preventDefault();
}