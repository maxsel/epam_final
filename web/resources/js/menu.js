$(document).ready(function () {
    $(".order-control").hide();
    $('#orderErrorMsg').hide();

    $('.to-order').change(function () {
        var id = $(this).attr("name");
        if (this.checked)
            $('#oc' + id).fadeIn('fast');
        else
            $('#oc' + id).fadeOut('fast');

    });
});

function minus(id) {
    var was = +$('.order-amount[name=' + id + ']').val();
    if (was > 1) {
        $('.order-amount[name=' + id + ']').val(was - 1);
    }
}
function plus(id) {
    var was = $('.order-amount[name=' + id + ']').val();
    if (+was < 10) {
        $('.order-amount[name=' + id + ']').val(+was + 1);
    }
}

function createJSON() {
    var jsonObj = [];
    $(".to-order:checked").each(function () {
        var id = $(this).attr("name");
        var amount = $('.order-amount[name=' + id + ']').val();
        var item = {};
        item["id"] = id;
        item["amount"] = amount;
        jsonObj.push(item);
    });
    return jsonObj;
}

$(document).on("submit", "#preorderForm", function (event) {
    event.preventDefault();
    $('#orderErrorMsg').hide();
    var $form = $(this);
    var order = createJSON();
    if( !$.isArray(order) || !order.length ) {
        $('#orderErrorMsg').show();
        return;
    }
    var params = {
        command: "preorder",
        orderJSON: JSON.stringify(order)
    };
    $.post($form.attr("action"), $.param(params), function (resp) {
        if (resp == "emptyOrder") {
            $('#orderErrorMsg').show();
        } else {
            window.location.href = resp;
        }
    });
});