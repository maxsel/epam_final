window.onload = function() {
    $('#emptyField').hide();
    $('#loginNotFree').hide();
    $('#passwordsNotMatch').hide();
    $('#invalidLogin').hide();
    $('#invalidPassword').hide();
};
$(document).on("submit", "#loginForm", function(event) {
    $('#emptyField').hide();
    $('#loginNotFree').hide();
    $('#passwordsNotMatch').hide();
    $('#invalidLogin').hide();
    $('#invalidPassword').hide();
    var $form = $(this);
    console.log($form.serialize());
    $.post($form.attr("action"), $form.serialize(), function(resp) {
        if (resp == "loginNotFree") {
            $('#loginNotFree').show();
        } else if (resp == "passwordsNotMatch") {
            $('#passwordsNotMatch').show();
        } else if (resp == "invalidLogin") {
            $('#invalidLogin').show();
        } else if (resp == "invalidPassword") {
            $('#invalidPassword').show();
        } else  if (resp == "emptyField") {
            $('#emptyField').show();
        } else {
            window.location.href = resp;
        }
    });
    event.preventDefault();
});