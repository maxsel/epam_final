window.onload = function () {
    $('#loginErrorMsg').hide();
}
$(document).on("submit", "#loginForm", function (event) {
    var $form = $(this);
    $.post($form.attr("action"), $form.serialize(), function (resp) {
        if (resp == "fail") {
            $('#loginErrorMsg').show();
        } else {
            window.location.href = resp;
        }
    });
    event.preventDefault();
});