function setOrderIdAndSubmit(id) {
    $('[name=orderId]').val(id);
    document.confirmForm.submit();
};

function setOrderIdAndDelete(id) {
    $('[name=orderId]').val(id);
    $('[name=command]').val("deleteorder");
    console.log($('[name=command]').val());
    document.confirmForm.submit();
};

function askForBill() {
    var params = {
        command: "askforbill"
    };
    $.post("/controller", $.param(params), function (resp) {
        window.location.href = resp;
    });
};