window.onload = function() {
    $('#errorMsg').hide();
    if ($("#dish_category").val() != null) {
        $("#input_category").val($("#dish_category").val());
    }
    if ($("#dish_status").val() != null) {
        $("#input_status").val($("#dish_status").val());
    }
};
$(document).on("submit", "#editForm", function (event) {
    var $form = $(this);
    $.post($form.attr("action"), $form.serialize(), function (resp) {
        if (resp == "fail") {
            $('#errorMsg').show();
        } else {
            window.location.href = resp;
        }
    });
    event.preventDefault();
});