<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${sessionScope.lang}" scope="session"/>
<fmt:setBundle basename="properties.pagecontent"/>
<html>
<head>
    <title><fmt:message key="error.title"/></title>
    <link rel="icon" href="/resources/favicon.ico">
    <link href="/resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/css/styles.css" rel="stylesheet">
    <link href="/resources/css/error.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="error-data">
        <h1><fmt:message key="error.header"/> </h1>
        <br/>
        <c:if test="${not empty pageContext.errorData.requestURI}">
        <i><fmt:message key="error.requesturi"/></i> ${pageContext.errorData.requestURI}
            <br/>
        </c:if>
        <c:if test="${not empty pageContext.errorData.servletName}">
            <i><fmt:message key="error.servletname"/></i> : ${pageContext.errorData.servletName}
            <br/>
        </c:if>
        <c:if test="${pageContext.errorData.statusCode != 0}">
            <i><fmt:message key="error.statuscode"/></i> : ${pageContext.errorData.statusCode}
            <br/>
        </c:if>
        <c:choose>
            <c:when test="${pageContext.errorData.throwable != null}">
                <i><fmt:message key="error.exception" /></i> : ${pageContext.errorData.throwable}
                <br/>
            </c:when>
            <c:when test="${exception != null}">
                    <i><fmt:message key="error.exception" /></i> : ${exception}
                <br/>
            </c:when>
        </c:choose>
        <c:choose>
            <c:when test="${not empty message}">
                <i><fmt:message key="error.message"/></i> : ${message}
                <br/>
            </c:when>
            <c:when test="${pageContext.errorData.statusCode == 404}">
                <i><fmt:message key="error.message"/></i> : <fmt:message key="error.msgnotfound" />
                <br/>
            </c:when>
        </c:choose>
        <br/>
        <a href="/controller"><fmt:message key="goto.main"/></a>
    </div>
</div>
</body>
</html>