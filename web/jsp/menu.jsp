<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<fmt:setLocale value="${sessionScope.lang}" scope="application"/>
<fmt:setBundle basename="properties.pagecontent"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 5 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/resources/favicon.ico">
    <title><fmt:message key="menu"/></title>
    <link href="/resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/css/styles.css" rel="stylesheet">
    <script src="/resources/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/js/menu.js"></script>
</head>
<body>
<c:import url="/jsp/fragment/navbar.jsp"/>
<div class="container">
    <br/><br/><br/><br/><br/><br/>
    <h1 style="text-align: center"><fmt:message key="menu"/></h1>
    <c:if test="${sessionScope.user.role.id == 1}">
        <a href="/controller?command=adddish"><fmt:message key="add.dish"/></a>
    </c:if>
    <form id="preorderForm" action="/controller" method="POST">
        <input type="hidden" name="command" value="preorder">
        <table class="table table-striped">
            <c:forEach items="${categorized}" var="category">
                <tr>
                    <th>${fn:toUpperCase(category.key)}</th>
                    <td></td><td></td><td></td>
                </tr>
                <c:forEach items="${category.value}" var="dish"
                           varStatus="loop">
                    <tr>
                        <c:if test="${sessionScope.user.role.id == 2}">
                            <div>
                                <td>
                                    <input type="checkbox"
                                           name="${dish.id}"
                                           class="to-order">
                                </td>
                            </div>
                        </c:if>
                        <td>${dish.getName()}
                            <c:if test="${not empty dish.getDescription()}">
                                <br/>${dish.getDescription()}
                            </c:if>
                            <br/>${dish.getServingSize()}
                            <c:if test="${sessionScope.user.role.id == 1}">
                                <br/>Status=${dish.getItemStatus()}
                                <br/>
                                <a href="/controller?command=editdish&id=${dish.id}">
                                    <fmt:message key="edit.dish"/>
                                </a> |
                                <c:if test="${dish.itemStatus.id == 2}">
                                    <a href="/controller?command=restoredish&id=${dish.id}">
                                        <fmt:message key="restore.dish"/></a>
                                </c:if>
                                <c:if test="${dish.itemStatus.id == 1}">
                                    <a href="/controller?command=deletedish&id=${dish.id}">
                                        <fmt:message key="delete.dish"/></a>
                                </c:if>

                            </c:if>
                        </td>

                        <c:if test="${sessionScope.user.role.id == 2}">
                            <td>
                                <div id="oc${dish.id}" class="order-control"
                                     style="display: none">
                                    <button type="button"
                                            onclick="minus(${dish.id})">-
                                    </button>
                                    <input class="order-amount" type="text"
                                           name="${dish.id}" value="1"
                                           size="3" style="text-align:center;"
                                           readonly>
                                    <button type="button"
                                            onclick="plus(${dish.id})">+
                                    </button>
                                </div>
                            </td>
                        </c:if>

                        <td><fmt:formatNumber type="number" value="${dish.getPrice()}"/>=</td>
                    </tr>
                </c:forEach><br>
            </c:forEach>

        </table>
        <c:if test="${sessionScope.user.role.id == 2}">
            <div id="orderErrorMsg" class="alert alert-error">
                <fmt:message key="no.items.chosen"/>
            </div>
            <button value="preorder" name="submit" id="submit"
                    class="btn btn-primary btn-lg btn-block"
                    type="submit"><fmt:message key="make.order"/></button>

        </c:if>
    </form>
</div>

<c:import url="/jsp/fragment/footer.jsp"/>

<c:import url="/jsp/fragment/scripts.jsp"/>

</body>
</html>