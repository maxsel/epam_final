<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ct" uri="customtags" %>
<fmt:setLocale value="${sessionScope.lang}" scope="session"/>
<fmt:setBundle basename="properties.pagecontent"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 5 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><fmt:message key="profile" /></title>
    <link rel="icon" href="/resources/favicon.ico">
    <link href="/resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/css/styles.css" rel="stylesheet">
</head>
<body>
<c:import url="/jsp/fragment/navbar.jsp"/>
<div class="container">
    <br/><br/><br/><br/><br/><h1><fmt:message key="profile" /></h1>
    <%--<div class="profile">
        <table>
            <tr><th><fmt:message key="login"/>:</th><td>${sessionScope.user.login}</td></tr>
            <tr><th><fmt:message key="first.name"/>:</th><td>${sessionScope.user.firstName}</td></tr>
            <tr><th><fmt:message key="last.name"/>:</th><td>${sessionScope.user.lastName}</td></tr>
            <c:if test="${sessionScope.user.role.id == 1}">
                <tr><th><fmt:message key="role"/>:</th><td>${sessionScope.user.role}</td></tr>
            </c:if>
            <tr><th><fmt:message key="balance"/>:</th><td><fmt:formatNumber value="${sessionScope.user.balance}" type="number"/></td></tr>
        </table>
    </div>--%>
    <ct:profile user="${sessionScope.user}">${sessionScope.lang}</ct:profile>
    <br/><br/><br/>
    <form id="loginForm" action="/controller" method="POST">
        <input type="hidden" name="command" value="moremoney">
        <div align="center">
            <button value="submit" name="submit"
                    class="btn btn-lg btn-primary"
                    type="submit">
                <fmt:message key="more.money"/>
            </button>
        </div>
    </form>
</div>

<c:import url="/jsp/fragment/footer.jsp" />

<c:import url="/jsp/fragment/scripts.jsp" />

</body>
</html>