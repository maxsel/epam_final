<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.lang}" scope="session"/>
<fmt:setBundle basename="properties.pagecontent"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 5 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/resources/favicon.ico">
    <title><fmt:message key="index.title"/></title>
    <link href="/resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/css/styles.css" rel="stylesheet">
    <script src="/resources/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/js/login.js"></script>
</head>
<body>
<c:import url="/jsp/fragment/navbar.jsp"/>
<div class="container">
    <div id="login-overlay" class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">
                    <b><fmt:message key="login.header"/></b>
                </h4>
            </div>
            <div class="modal-body">
                <form id="loginForm" class="form-signin" action="/controller"
                      method="POST">
                    <input type="hidden" name="command" value="login">
                    <div class="well">
                        <div id="loginErrorMsg" class="alert alert-error">
                            <fmt:message key="wrong.username"/></div>
                        <div class="form-group required">
                            <label class="control-label">
                                <fmt:message key="required.fields" /></label>
                        </div>
                        <div class="form-group required">
                            <label for="inputLogin"
                                   class="control-label"><fmt:message
                                    key="label.login"/></label>
                            <input type="text" class="form-control"
                                   id="inputLogin" name="input_login" value=""
                                   required=""
                                   title="<fmt:message key="please.login" />"
                                   placeholder=<fmt:message key="label.login"/> />
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group required">
                            <label for="inputPassword"
                                   class="control-label"><fmt:message
                                    key="label.password"/></label>
                            <input type="password" class="form-control"
                                   id="inputPassword" name="input_password"
                                   placeholder=
                                   "<fmt:message key="label.password"/>"
                                   value="" required=""
                                   title="<fmt:message key="please.password" />" />
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <button value="login" name="submit"
                            class="btn btn-lg btn-primary btn-block"
                            type="submit"><fmt:message key="log.in"/>
                    </button>
                </form>

            </div>
        </div>

    </div>
</div> <!-- /container -->

<c:import url="/jsp/fragment/footer.jsp"/>

<c:import url="/jsp/fragment/scripts.jsp"/>

</body>
</html>