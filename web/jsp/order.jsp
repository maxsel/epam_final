<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.lang}" scope="session"/>
<fmt:setBundle basename="properties.pagecontent"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 5 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/resources/favicon.ico">
    <title><fmt:message key="submit.order"/></title>
    <link href="/resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/css/styles.css" rel="stylesheet">
    <script src="/resources/jquery/jquery.min.js"></script>
</head>
<body>
<c:import url="/jsp/fragment/navbar.jsp"/>
<div class="container">
    <div id="login-overlay" class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">
                    <b><fmt:message key="submit.order"/></b>
                </h4>
            </div>
            <div class="modal-body">
                <form id="orderForm" class="form-signin" action="/controller"
                      method="POST">
                    <input type="hidden" name="command" value="order">
                    <div>
                    <fmt:message key="your.order"/>:<br/>
                        <ol>
                            <c:forEach items="${sessionScope.ordered}" var="item">
                                <li>
                                    ${item.getDish().getName()} : ${item.getAmount()}
                                </li>
                            </c:forEach>
                        </ol>
                    </div>
                    <button value="order" name="submit"
                            class="btn btn-lg btn-primary"
                            type="submit"><fmt:message key="submit.order"/>
                    </button>
                    <button value="cancel" name="cancel"
                            onclick="location.href='/controller?command=cancelorder';"
                            class="btn btn-lg"
                            type="button"><fmt:message key="cancel.order"/>
                    </button>
                </form>
            </div>
        </div>
    </div>
</div> <!-- /container -->

<c:import url="/jsp/fragment/footer.jsp"/>

<c:import url="/jsp/fragment/scripts.jsp"/>

</body>
</html>