<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.lang}" scope="session"/>
<fmt:setBundle basename="properties.pagecontent"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 5 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/resources/favicon.ico">
    <title><fmt:message key="sign.up"/></title>
    <link href="/resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/css/styles.css" rel="stylesheet">
    <script src="/resources/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/js/signup.js"></script>
</head>
<body>
<c:import url="/jsp/fragment/navbar.jsp"/>
<div class="container">
    <div id="login-overlay" class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">
                    <b><fmt:message key="signup.header"/></b>
                </h4>
            </div>
            <div class="modal-body">

                <form id="loginForm" class="form-signin" action="/controller" method="POST">
                    <input type="hidden" name="command" value="signup">

                    <div class="well">
                        <div id="loginNotFree"
                             class="alert alert-error"><fmt:message key="login.not.free"/></div>
                        <div id="passwordsNotMatch"
                             class="alert alert-error"><fmt:message key="passwords.not.match"/></div>
                        <div id="invalidLogin"
                             class="alert alert-error"><fmt:message key="invalid.login"/></div>
                        <div id="invalidPassword"
                             class="alert alert-error"><fmt:message key="invalid.password"/></div>
                        <div id="emptyField"
                             class="alert alert-error"><fmt:message key="empty.field"/></div>
                        <div class="form-group required">
                            <label class="control-label"><fmt:message key="required.fields" /></label>
                        </div>
                        <div class="form-group required">
                            <label for="input_login"
                                   class="control-label"><fmt:message key="label.login" /></label>
                            <input type="text" class="form-control"
                                   id="input_login" name="input_login" value=""
                                   required="required" pattern="^[A-z0-9_-]{3,15}$"
                                   title="<fmt:message key="invalid.login" />"
                                   placeholder="<fmt:message key="label.login" />">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group required">
                            <label for="input_password"
                                   class="control-label"><fmt:message key="label.password" /></label>
                            <input type="password" class="form-control"
                                   id="input_password" name="input_password"
                                   placeholder="<fmt:message key="label.password" />"
                                   value="" required="required" pattern="^.{6,16}$"
                                   title="<fmt:message key="invalid.password" />">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group required">
                            <label for="repeat_password"
                                   class="control-label"><fmt:message key="label.repeat.password" /></label>
                            <input type="password" class="form-control"
                                   id="repeat_password" name="repeat_password"
                                   placeholder="<fmt:message key="label.repeat.password" />"
                                   value=""
                                   required="required"
                                   title="<fmt:message key="please.repeat.password" />">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group required">
                            <label for="input_first_name"
                                   class="control-label"><fmt:message key="first.name" /></label>
                            <input type="text" class="form-control"
                                   id="input_first_name" name="input_first_name"
                                   placeholder="<fmt:message key="first.name" />" value=""
                                   required="required"
                                   title="<fmt:message key="please.first.name" />">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group required">
                            <label for="input_last_name"
                                   class="control-label"><fmt:message key="last.name" /></label>
                            <input type="text" class="form-control"
                                   id="input_last_name" name="input_last_name"
                                   placeholder="<fmt:message key="last.name" />" value=""
                                   required="required"
                                   title="<fmt:message key="please.last.name" />">
                            <span class="help-block"></span>
                        </div>

                    </div>
                    <button value="signup" name="submit" id="submit"
                            class="btn btn-lg btn-primary btn-block"
                            type="submit"><fmt:message key="sign.up" /></button>
                </form>

            </div>
        </div>

    </div>
</div> <!-- /container -->

<c:import url="/jsp/fragment/footer.jsp"/>

<c:import url="/jsp/fragment/scripts.jsp"/>

</body>
</html>