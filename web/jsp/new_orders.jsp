<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<fmt:setLocale value="${sessionScope.lang}" scope="session"/>
<fmt:setBundle basename="properties.pagecontent"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 5 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/resources/favicon.ico">
    <title><fmt:message key="new.orders"/></title>
    <link href="/resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/css/styles.css" rel="stylesheet">
    <script src="/resources/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/js/new_orders.js"></script>
</head>
<body>
<c:import url="/jsp/fragment/navbar.jsp"/>
<div class="container">
    <br/><br/><br/><br/>
    <div align="center"><h1><fmt:message key="new.orders"/></h1></div>
    <br/><br/>
    <c:if test="${sessionScope.user.role.id == 2 and confirmed.size() != 0}">
        <button value="askbill" name="submit" id="submit"
                class="btn btn-lg btn-primary btn-block"
                type="button"
                onclick="askForBill()"> <fmt:message key="ask.bill"/>
        </button>
    </c:if>
    <c:if test="${sessionScope.user.role.id == 2 and confirmed.size() == 0}">
        <button value="askbill" name="submit" id="submit"
                class="btn btn-lg btn-block"
                type="button" disabled="disabled">
                <fmt:message key="no.confirmed.orders"/>
        </button>
    </c:if>
    <form id="confirmForm" name="confirmForm" action="/controller" method="POST">
        <input type="hidden" name="command" value="confirmorder">
        <input type="hidden" name="orderId">
        <input type="hidden" name="confirmed" value="${confirmed}">
        <table class="table table-striped">
            <c:forEach items="${orders}" var="order">
                <tr>
                    <th>${order.key.orderDate.toString().replace("T"," ")}
                        : ${order.key.client.login}
                        : ${order.key.orderStatus}</th>
                </tr>
                <tr>
                    <td>
                        <ol>
                            <c:forEach items="${order.value}" var="dish"
                                       varStatus="loop">
                                <li>${dish.dish.name} : ${dish.amount}</li>
                            </c:forEach>
                        </ol>
                        <c:if test="${sessionScope.user.role.id == 1 and order.key.orderStatus.id == 1}">
                            <button name="confirm" value="${order.key.id}"
                                    onclick="{setOrderIdAndSubmit(${order.key.id});}"
                                    class="btn btn-primary"
                                    type="submit"><fmt:message key="confirm.order"/>
                            </button>
                        </c:if>
                        <c:if test="${sessionScope.user.role.id == 2 and order.key.orderStatus.id == 1}">
                            <button name="confirm" value="${order.key.id}"
                                    onclick="{setOrderIdAndDelete(${order.key.id});}"
                                    class="btn btn-primary"
                                    type="submit"><fmt:message key="delete.order"/>
                            </button>
                        </c:if>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </form>
</div>

<c:import url="/jsp/fragment/footer.jsp"/>

<c:import url="/jsp/fragment/scripts.jsp"/>

</body>
</html>