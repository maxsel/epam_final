<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<fmt:setLocale value="${sessionScope.lang}" scope="application"/>
<fmt:setBundle basename="properties.pagecontent"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 5 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/resources/favicon.ico">
    <title><fmt:message key="bill.paid"/></title>
    <link href="/resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/css/styles.css" rel="stylesheet">
    <script src="/resources/jquery/jquery.min.js"></script>
</head>
<body>
<c:import url="/jsp/fragment/navbar.jsp"/>
<div class="container">
    <br/><br/><br/><br/>
    <form id="preorderForm" action="/controller" method="POST">
        <input type="hidden" name="command" value="preorder">
        <table class="table table-striped">
            <c:forEach items="${orders}" var="bill">
                <tr>
                    <th>${bill.key.orderDate} : ${bill.key.orderStatus}</th>
                </tr>
                <tr>
                    <td>
                        <ol>
                            <c:forEach items="${bill.value}" var="dish"
                                       varStatus="loop">
                                <li>${dish.dish.name} : ${dish.amount}</li>
                            </c:forEach>
                        </ol>
                        <c:if test="${sessionScope.user.role.id == 1}">
                            <button value="confirm" name="submit"
                                    class="btn btn-lg btn-primary"
                                    type="submit"><fmt:message key="confirm.order"/>
                            </button>
                        </c:if>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </form>
</div>

<c:import url="/jsp/fragment/footer.jsp"/>

<c:import url="/jsp/fragment/scripts.jsp"/>

</body>
</html>