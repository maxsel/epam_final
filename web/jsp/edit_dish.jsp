<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.lang}" scope="session"/>
<fmt:setBundle basename="properties.pagecontent"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 5 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/resources/favicon.ico">
    <title>Carousel Template for Bootstrap</title>
    <!-- Bootstrap core CSS -->
    <link href="/resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="/resources/css/styles.css" rel="stylesheet">
    <script src="/resources/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/js/edit_dish.js"></script>
</head>
<body>
<c:import url="/jsp/fragment/navbar.jsp"/>
<div class="container">
    <div id="login-overlay" class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">
                    <b><fmt:message key="editing.dish"/></b>
                </h4>
            </div>
            <div class="modal-body">
                <form id="editForm" class="form-signin" action="/controller" method="POST">
                    <input type="hidden" name="command" value="savedish">
                    <input type="hidden" id="dish_id" name="dish_id" value="${dish.id}">
                    <input type="hidden" id="dish_status" name="dish_status" value="${dish.itemStatus.id}">
                    <input type="hidden" id="dish_category" name="dish_category" value="${dish.category.id}">
                    <input type="hidden" id="dish_description" name="dish_description" value="${dish.description}">
                    <div class="well">
                        <div id="errorMsg" class="alert alert-error">
                            <fmt:message key="wrong.input"/></div>
                        <div class="form-group required">
                            <label for="input_name"
                                   class="control-label"><fmt:message key="label.name" /></label>
                            <input type="text" class="form-control"
                                   id="input_name" name="input_name" value="${dish.name}"
                                   required="required"
                                   title="<fmt:message key="invalid.name" />"
                                   placeholder="<fmt:message key="label.name" />">
                        </div>
                        <div class="form-group">
                            <label for="input_description"
                                   class="control-label"><fmt:message key="description" /></label>
                            <textarea class="form-control"
                                      id="input_description"
                                      name="input_description">${dish.description}</textarea>
                        </div>
                        <div class="form-group required">
                            <label for="input_price"
                                   class="control-label"><fmt:message key="label.price" /></label>
                            <input type="text" class="form-control"
                                   id="input_price" name="input_price"
                                   placeholder="<fmt:message key="label.price" />"
                                   value="${dish.price}" required="required" pattern="^[0-9]*$"
                                   title="<fmt:message key="invalid.price" />">
                        </div>
                        <div class="form-group required">
                            <label for="input_status"
                                   class="control-label">Status</label>
                            <select id="input_status" name="input_status">
                                <option disabled>Select item status</option>
                                <option selected value="1">PRESENT</option>
                                <option value="2">NOT PRESENT</option>
                            </select>
                        </div>
                        <div class="form-group required">
                            <label for="input_category"
                                   class="control-label">Category</label>
                            <select id="input_category" name="input_category">
                                <option disabled>Select item category</option>
                                <c:forEach items="${categories}" var="category">
                                    <option value="${category.id}">${category.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="form-group required">
                            <label for="input_size"
                                   class="control-label"><fmt:message key="label.size" /></label>
                            <input type="text" class="form-control"
                                   id="input_size" name="input_size"
                                   placeholder="<fmt:message key="label.size" />"
                                   value="${dish.servingSize}" required="required" pattern="^[0-9]*$"
                                   title="<fmt:message key="invalid.size" />">
                        </div>

                    </div>
                    <button value="save" name="save" id="save"
                            class="btn btn-lg btn-primary btn-block"
                            type="submit"><fmt:message key="save.dish" /></button>
                </form>

            </div>
        </div>

    </div>
</div> <!-- /container -->

<c:import url="/jsp/fragment/footer.jsp"/>

<c:import url="/jsp/fragment/scripts.jsp"/>

</body>
</html>