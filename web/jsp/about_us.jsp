<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.lang}" scope="session"/>
<fmt:setBundle basename="properties.pagecontent"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 5 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/resources/favicon.ico">
    <title><fmt:message key="about.us"/></title>
    <link href="/resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/css/styles.css" rel="stylesheet">
    <script src="/resources/jquery/jquery.min.js"></script>
</head>
<body>
<c:import url="/jsp/fragment/navbar.jsp"/>
<div class="container about_us">
    <br/><br/><br/><br/><br/><br/>
    <h1 style="text-align: center">
        <fmt:message key="about0"/>
    </h1>
    <p style="text-align: center">
        <fmt:message key="about1"/><br/>
        <fmt:message key="about2"/>
    </p>
    <p style="text-align: center">
        <fmt:message key="about3"/><br/>
        <fmt:message key="about4"/>
    </p>
    <p style="text-align: center">
        <fmt:message key="about5"/><br/>
        <fmt:message key="about6"/><br/>
        <fmt:message key="about7"/>
    </p>
    <br/>
    <h3 style="text-align: center">
        <fmt:message key="about8"/>
    </h3>
    <h4 style="text-align: center; font-style: italic">
        <fmt:message key="about9"/>
    </h4>
    <p style="text-align: center">
        <fmt:message key="about10"/><br/>
        <fmt:message key="about11"/><br/>
        <fmt:message key="about12"/><br/>
        <br/>
        <fmt:message key="about13"/><br/>
        <br/>
        <fmt:message key="about14"/><br/>
    </p>
</div>

<c:import url="/jsp/fragment/footer.jsp" />

<c:import url="/jsp/fragment/scripts.jsp" />

</body>
</html>