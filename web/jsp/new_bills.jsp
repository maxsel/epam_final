<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<fmt:setLocale value="${sessionScope.lang}" scope="session"/>
<fmt:setBundle basename="properties.pagecontent"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 5 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/resources/favicon.ico">
    <title><fmt:message key="new.bills"/></title>
    <link href="/resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/css/styles.css" rel="stylesheet">
    <script src="/resources/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/js/new_bills.js"></script>
</head>
<body>
<c:import url="/jsp/fragment/navbar.jsp"/>
<div class="container">
    <br/><br/><br/><br/>
    <div align="center"><h1><fmt:message key="new.bills"/></h1></div>
    <br/><br/>
    <form id="confirmForm" name="confirmForm" action="/controller" method="POST">
        <input type="hidden" name="command">
        <input type="hidden" name="billIdToConfirm">
        <input type="hidden" name="billIdToPay">
        <div id="errorMsg" class="alert alert-error">
            <fmt:message key="insufficient.funds"/></div>
        <table class="table table-striped">
            <c:forEach items="${mapbills}" var="mapbill">
                <tr>
                    <th>${mapbill.key.creationDate.toString().replace("T"," ")}
                        : ${mapbill.key.client.login}
                        : ${mapbill.key.status}</th>
                </tr>
                <tr>
                    <td>
                        <ol>
                            <c:forEach items="${mapbill.value}" var="dish"
                                       varStatus="loop">
                                <li>
                                    ${dish.dish.name} (x${dish.amount})
                                    : <fmt:formatNumber type="number"
                                                        value="${dish.instantBill}"/>
                                </li>
                            </c:forEach>
                        </ol>
                        <p>
                            <fmt:message key="total"/>:
                            <fmt:formatNumber type="number"
                                              value="${costs.get(mapbill.key.id)}"/>
                        </p>
                        <c:if test="${sessionScope.user.role.id == 1 and mapbill.key.status.id == 1}">
                            <button name="confirm" value="${mapbill.key.id}"
                                    onclick="{setBillIdAndConfirm(${mapbill.key.id});}"
                                    class="btn btn-primary"
                                    type="submit"><fmt:message key="confirm.bill"/>
                            </button>
                        </c:if>
                        <c:if test="${sessionScope.user.role.id == 2 and mapbill.key.status.id == 2}">
                            <button name="confirm" value="${mapbill.key.id}"
                                    onclick="{setBillIdAndPay(${mapbill.key.id});}"
                                    class="btn btn-primary"
                                    type="submit"><fmt:message key="pay.bill"/>
                            </button>
                        </c:if>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </form>
</div>

<c:import url="/jsp/fragment/footer.jsp"/>

<c:import url="/jsp/fragment/scripts.jsp"/>

</body>
</html>