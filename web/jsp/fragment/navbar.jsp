<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ct" uri="customtags" %>
<fmt:setLocale value="${sessionScope.lang}" scope="session"/>
<fmt:setBundle basename="properties.pagecontent"/>
<html>
<head>
    <title>Navbar</title>
</head>
<body>
<!-- NAVBAR
================================================== -->
<div class="navbar-wrapper">
    <div class="container">
        <nav class="navbar navbar-inverse navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed"
                            data-toggle="collapse" data-target="#navbar"
                            aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only"><fmt:message key="toggle.navigation"/></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/controller?command=empty">
                        <fmt:message key="project.name"/>
                    </a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="/controller">
                            <fmt:message key="home"/></a></li>
                        <li><a href="/controller?command=showmenu">
                            <fmt:message key="menu"/></a></li>
                        <li><a href="/controller?command=showaboutus">
                            <fmt:message key="about.us"/></a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                               role="button" aria-haspopup="true" aria-expanded="false">
                                <fmt:message key="language"/><span class="caret"/>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="/controller?command=changelanguage&lang=en_US">
                                        <span><img src="/resources/img/en.gif"
                                                   width="22" height="14" alt="en"></span>
                                        <fmt:message key="lang.en"/></a></li>
                                <li><a href="/controller?command=changelanguage&lang=ru_RU">
                                        <span><img src="/resources/img/ru.gif"
                                                   width="22" height="14" alt="ru"></span>
                                    <fmt:message key="lang.ru"/></a></li>
                                <li><a href="/controller?command=changelanguage&lang=be_BY">
                                    <span><img src="/resources/img/be.png"
                                               width="22" height="14" alt="be"></span>
                                        <fmt:message key="lang.be"/></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <div style='position: relative; float: right;'>
                        <ul class="nav navbar-nav">
                            <c:choose>
                                <c:when test="${sessionScope.user != null}">
                                    <li class="dropdown">
                                        <%--<a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                           role="button" aria-haspopup="true" aria-expanded="false">
                                            <span class="glyphicon glyphicon-user"/>
                                            ${sessionScope.user.login}<span class="caret"/>
                                        </a>--%>
                                        <ct:dropdown-user user="${sessionScope.user}"/>
                                        <ul class="dropdown-menu">
                                            <li><a href="/controller?command=showprofile">
                                                <fmt:message key="profile"/></a></li>
                                            <li><a href="/controller?command=showneworders">
                                                <fmt:message key="new.orders"/></a></li>
                                            <li><a href="/controller?command=shownewbills">
                                                <fmt:message key="new.bills"/></a></li>
                                            <li><a href="/controller?command=signout">
                                                <fmt:message key="sign.out"/></a></li>
                                            <c:if test="${ordered != null}">
                                                <li role="separator" class="divider"></li>
                                                <%--<li class="dropdown-header">Nav header</li>--%>
                                                <li><a style="color: red" href="/controller?command=showorder">
                                                    <fmt:message key="unsubmitted.order"/></a></li>
                                            </c:if>
                                        </ul>
                                    </li>
                                </c:when>
                                <c:otherwise>
                                    <ul class="nav navbar-nav">
                                        <li><a href="/controller?command=showloginpage">
                                            <fmt:message key="log.in"/></a></li>
                                        <li><a href="/controller?command=showsignuppage">
                                            <fmt:message key="sign.up"/></a></li>
                                    </ul>
                                </c:otherwise>
                            </c:choose>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</div>
</body>
</html>
