<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ct" uri="customtags" %>
<fmt:setLocale value="${sessionScope.lang}" scope="session"/>
<fmt:setBundle basename="properties.pagecontent"/>
<html>
<head>
    <title>Footer</title>
</head>
<body>
<!-- FOOTER -->
<footer id="footer" class="footer">
    <div class="container">
        <hr/>
        <%--<p class="pull-right"><a href="#"><fmt:message key="to.top"/></a></p>
        <p><fmt:message key="footer.msg"/></p>--%>
        <ct:footer/>
    </div>
</footer>
</body>
</html>
