<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${sessionScope.lang}" scope="session"/>
<fmt:setBundle basename="properties.pagecontent"/>
<html>
<head>
    <title>Carousel</title>
</head>
<body>
<!-- Carousel
================================================== -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <img class="first-slide" src="/resources/img/slide1.jpg"
                 alt="<fmt:message key="slide1.h1"/>">
            <div class="container">
                <div class="carousel-caption">
                    <h1><fmt:message key="slide1.h1"/></h1>
                    <p><fmt:message key="slide1.text"/></p>
                    <p><a class="btn btn-lg btn-primary"
                          href="/controller?command=showaboutus"
                          role="button"><fmt:message key="slide1.button"/></a></p>
                </div>
            </div>
        </div>
        <div class="item">
            <img class="second-slide" src="/resources/img/slide2.jpg"
                 alt="<fmt:message key="slide2.h1"/>">
            <div class="container">
                <div class="carousel-caption">
                    <h1><fmt:message key="slide2.h1"/></h1>
                    <p><fmt:message key="slide2.text"/></p>
                    <p><a class="btn btn-lg btn-primary"
                          href="/controller?command=showmenu"
                          role="button"><fmt:message key="slide2.button"/></a></p>
                </div>
            </div>
        </div>
        <div class="item">
            <img class="third-slide" src="/resources/img/slide3.jpg"
                 alt="Third slide">
            <div class="container">
                <div class="carousel-caption">
                    <h1><fmt:message key="slide3.h1"/></h1>
                    <p><fmt:message key="slide3.text"/></p>
                    <p><a class="btn btn-lg btn-primary"
                          href="/controller?command=showsignuppage"
                          role="button"><fmt:message key="slide3.button"/></a></p>
                </div>
            </div>
        </div>
    </div>
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only"><fmt:message key="previous.slide"/></span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only"><fmt:message key="next.slide"/></span>
    </a>
</div><!-- /.carousel -->
</body>
</html>
