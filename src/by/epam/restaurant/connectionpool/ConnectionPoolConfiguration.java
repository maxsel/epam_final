package by.epam.restaurant.connectionpool;

import java.util.ResourceBundle;

/**
 * Class incapsulating logic of getting {@link ConnectionPool's} init parameters
 * from {@code properties} files.
 */
class ConnectionPoolConfiguration {
    private static final String PATH_TO_PROPERTIES = "properties.database";
    private static ResourceBundle resourceBundle = ResourceBundle.getBundle(PATH_TO_PROPERTIES);
    private static String url = getProperty("url");
    private static String user = getProperty("user");
    private static String password = getProperty("password");
    private static String poolCapacity = getProperty("pool.capacity");
    private static String timeWait = getProperty("time.wait");

    private ConnectionPoolConfiguration() {
    }

    /**
     * @return URL of database
     */
    static String getUrl() {
        return url;
    }

    /**
     * @return database user's name
     */
    static String getUser() {
        return user;
    }

    /**
     * @return database user's password
     */
    static String getPassword() {
        return password;
    }

    /**
     * @return capacity of {@link ConnectionPool}
     */
    static int getPoolCapacity() {
        return Integer.valueOf(poolCapacity);
    }

    /**
     * @return time, after which {@link ConnectionPool} stops waiting
     * for getting {@code Connection} from {@code DriverManager}
     */
    static int getTimeWait() {
        return Integer.valueOf(timeWait);
    }

    /**
     * @param key key of needed entry in properties file
     * @return required value from properties file
     */
    private static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}
