package by.epam.restaurant.connectionpool;

/**
 * Exception which is thrown when any problems with
 * the work of {@link ConnectionPool} arise.
 */
public class ConnectionPoolException extends Exception {
    private static final long serialVersionUID = 7649457865021273223L;

    ConnectionPoolException() {
    }

    ConnectionPoolException(String message) {
        super(message);
    }

    ConnectionPoolException(String message, Throwable cause) {
        super(message, cause);
    }

    ConnectionPoolException(Throwable cause) {
        super(cause);
    }

    public ConnectionPoolException(String message,
                                   Throwable cause,
                                   boolean enableSuppression,
                                   boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
