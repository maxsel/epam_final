package by.epam.restaurant.validator;

/**
 * Validates user's input for editing Dish from webpage
 */
public class DishValidator {
    private static final String NUMERIC_REGEX = "^[0-9]+$";

    /**
     * Defines if all given values are valid and can be used
     * as {@code Dish}'s fields.
     * @param name name provided by user
     * @param price price provided by user
     * @param status status provided by user
     * @param category category provided by user
     * @param size size provided by user
     * @return true, if all provided values are valid
     */
    public static boolean isValid(String name, String price, String status,
                                  String category, String size) {
        return name != null && !name.isEmpty()
                && price != null && !price.isEmpty() && price.matches(NUMERIC_REGEX)
                && size != null && !size.isEmpty() && size.matches(NUMERIC_REGEX)
                && status != null && !status.isEmpty() && status.matches(NUMERIC_REGEX)
                && category != null && !category.isEmpty() && category.matches(NUMERIC_REGEX);
    }
}
