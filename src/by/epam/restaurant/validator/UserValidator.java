package by.epam.restaurant.validator;

import java.util.regex.Pattern;

/**
 * Validates user's input for creating new User from webpage
 */
public class UserValidator {
    private static final String LOGIN_PATTERN = "^[A-z0-9_-]{3,15}$";

    /**
     * Defines if provided login is valid
     * @param login login provided by user
     * @return true if given login contains only latin letter (both cases),
     * underscore and dash and is 3-15 characters length
     */
    public static boolean validLogin(String login) {
        return Pattern.matches(LOGIN_PATTERN, login);
    }

    /**
     * Defines if provided password is valid
     * @param password password provided by user
     * @return true, if given password is 6-16 characters length
     */
    public static boolean validPassword(String password) {
        return password.length() >= 6 && password.length() <= 16;
    }
}
