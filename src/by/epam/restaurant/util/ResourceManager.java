package by.epam.restaurant.util;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * ResourceManager's instance is used to get values from properties files
 */
public enum ResourceManager {
    INSTANCE;

    private final String RESOURCE_NAME = "properties.pagecontent";
    private ResourceBundle resourceBundle;

    ResourceManager() {
        resourceBundle = ResourceBundle.getBundle(RESOURCE_NAME, Locale.getDefault());
    }

    /**
     * Changes concrete properties file from bundle with user's messages.
     * @param locale {@link Locale} locale, defining concrete properties file
     *                             with localized messages
     */
    public void changeResource(Locale locale) {
        resourceBundle = ResourceBundle.getBundle(RESOURCE_NAME, locale);
    }

    /**
     * Gets a value from properties file.
     * @param key key of properties file's entry to be returned
     * @return required value with the given key
     */
    public String getString(String key) {
        return resourceBundle.getString(key);
    }
}