package by.epam.restaurant.dao;

import by.epam.restaurant.entity.User;
import by.epam.restaurant.entity.UserRole;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Data Access Object for writing {@link User}'s from/into database
 */
public class UserDAO extends AbstractDAO {
    private static final String INSERT_USER
            = "INSERT INTO USERS (LOGIN, PASSWORD, FIRST_NAME, LAST_NAME, ROLE_ID, BALANCE) " +
            "VALUES (?, ?, ?, ?, ?, ?)";
    private static final String SELECT_USER_BY_LOGIN_PASSWORD
            = "SELECT * FROM USERS WHERE LOGIN=? AND PASSWORD=?";
    private static final String SELECT_USER_BY_ID = "SELECT * FROM USERS WHERE USER_ID=?";
    private static final String SELECT_USER_BY_LOGIN = "SELECT * FROM USERS WHERE LOGIN=?";
    private static final String UPDATE = "UPDATE USERS " +
            "SET LOGIN=?, PASSWORD=?, FIRST_NAME=?, LAST_NAME=?, ROLE_ID=?, BALANCE=? " +
            "WHERE USER_ID=?";
    private static final String DELETE_USER_BY_ID = "DELETE FROM USERS WHERE USER_ID=?";

    public UserDAO() throws DAOException {
    }

    /**
     * Finds user with given login and password.
     * @param login id of user to look for
     * @param password password of user to look for
     * @return user with given credentials or null if it doesn't exist
     * @throws DAOException
     */
    public User findByCredentials(String login, String password)
            throws DAOException {
        try (PreparedStatement ps =
                     connection.prepareStatement(SELECT_USER_BY_LOGIN_PASSWORD)) {
            ps.setString(1, login);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return new User(rs.getLong("USER_ID"), rs.getString("LOGIN"),
                        rs.getString("PASSWORD"), rs.getString("FIRST_NAME"),
                        rs.getString("LAST_NAME"),
                        UserRole.fromLong(rs.getLong("ROLE_ID")),
                        rs.getLong("BALANCE"));
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new DAOException("Error in findByCredentials()", e);
        }
    }

    /**
     * Finds user with given id.
     * @param id id of user to look for
     * @return user with given id or null if it doesn't exist
     * @throws DAOException
     */
    public User findById(long id) throws DAOException {
        try (PreparedStatement ps = connection.prepareStatement(SELECT_USER_BY_ID)) {
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return new User(rs.getLong("USER_ID"), rs.getString("LOGIN"),
                        rs.getString("PASSWORD"), rs.getString("FIRST_NAME"),
                        rs.getString("LAST_NAME"), UserRole.fromLong(rs.getLong("ROLE_ID")),
                        rs.getLong("BALANCE"));
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new DAOException("Error in findById()", e);
        }
    }

    /**
     * Defines if user with such login already exists
     * @param login login
     * @return true if user with such login already exists
     * @throws DAOException
     */
    public boolean isLoginFree(String login) throws DAOException {
        try (PreparedStatement ps = connection.prepareStatement(SELECT_USER_BY_LOGIN)) {
            ps.setString(1, login);
            ResultSet rs = ps.executeQuery();
            return !rs.next();
        } catch (SQLException e) {
            throw new DAOException("Error in isLoginFree()", e);
        }
    }


    /**
     * Deletes given user from database by id
     * @param user user to deleteAllOfOrder
     * @throws DAOException
     */
    public void delete(User user) throws DAOException {
        try (PreparedStatement ps = connection.prepareStatement(DELETE_USER_BY_ID)) {
            ps.setLong(1, user.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Error in deleteAllOfOrder()", e);
        }
    }

    /**
     * Writes user into database
     * @param user user to persist in DB
     * @return this user itself with id field correctly set
     * @throws DAOException
     */
    public User registerNewUser(User user) throws DAOException {
        String generated[] = {"USER_ID"};
        try (PreparedStatement ps = connection.prepareStatement(INSERT_USER, generated)) {
            ps.setString(1, user.getLogin());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getFirstName());
            ps.setString(4, user.getLastName());
            ps.setLong(5, user.getRole().getId());
            ps.setLong(6, user.getBalance());
            ps.executeUpdate();
            ResultSet generatedKeys = ps.getGeneratedKeys();
            if (generatedKeys.next()) {
                user.setId(generatedKeys.getLong(1));
            } else {
                throw new DAOException("No ID obtained");
            }
            return user;
        } catch (SQLException e) {
            throw new DAOException("Error in registerNewUser()", e);
        }
    }

    /**
     * Updates user in database
     * @param user user to update in DB
     * @throws DAOException
     */
    public User update(User user) throws DAOException {
        try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
            ps.setString(1, user.getLogin());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getFirstName());
            ps.setString(4, user.getLastName());
            ps.setLong(5, user.getRole().getId());
            ps.setLong(6, user.getBalance());
            ps.setLong(7, user.getId());
            ps.executeUpdate();
            ps.executeUpdate();
            return user;
        } catch (SQLException e) {
            throw new DAOException("Error in update()", e);
        }
    }
}