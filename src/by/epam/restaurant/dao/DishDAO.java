package by.epam.restaurant.dao;

import by.epam.restaurant.entity.Dish;
import by.epam.restaurant.entity.ItemStatus;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Data Access Object for writing {@link Dish}'s from/into database
 */
public class DishDAO extends AbstractDAO {
    private static final String SELECT_ALL = "SELECT * FROM ITEMS ORDER BY ITEM_ID";
    private static final String SELECT_BY_ID = "SELECT * FROM ITEMS WHERE ITEM_ID=?";
    private static final String INSERT_DISH =
            "INSERT INTO ITEMS (ITEM_NAME, ITEM_PRICE, ITEM_DESCRIPTION," +
                    " ITEM_STATUS, ITEM_CATEGORY, SERVING_SIZE) " +
                    "VALUES (?, ?, ?, ?, ?, ?)";
    private static final String UPDATE_DISH = "UPDATE ITEMS " +
                    "SET ITEM_NAME=?, ITEM_PRICE=?, ITEM_DESCRIPTION=?, " +
                    "ITEM_STATUS=?, ITEM_CATEGORY=?, SERVING_SIZE=? " +
                    "WHERE ITEM_ID=?";

    public DishDAO() throws DAOException {
    }

    /**
     * Retrieves all dishes from database
     * @return List with all dishes
     * @throws DAOException
     */
    public List<Dish> findAll() throws DAOException {
        try (PreparedStatement ps = connection.prepareStatement(SELECT_ALL);
             CategoryDAO catDAO = new CategoryDAO()) {
            List<Dish> dishes = new ArrayList<>();
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Dish d = new Dish(rs.getLong("ITEM_ID"), rs.getString("ITEM_NAME"),
                        rs.getLong("ITEM_PRICE"), rs.getString("ITEM_DESCRIPTION"),
                        ItemStatus.fromLong(rs.getLong("ITEM_STATUS")),
                        catDAO.findById(rs.getLong("ITEM_CATEGORY")),
                        rs.getLong("SERVING_SIZE"));
                dishes.add(d);
            }
            return dishes;
        } catch (SQLException e) {
            throw new DAOException("Error in findAll()", e);
        }
    }

    /**
     * Finds dish with given id.
     * @param id id of dish to look for
     * @return dish with given id or null if it doesn't exist
     * @throws DAOException
     */
    public Dish findById(long id) throws DAOException {
        try (PreparedStatement ps = connection.prepareStatement(SELECT_BY_ID);
             CategoryDAO catDAO = new CategoryDAO()) {
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                return new Dish(rs.getLong("ITEM_ID"), rs.getString("ITEM_NAME"),
                        rs.getLong("ITEM_PRICE"), rs.getString("ITEM_DESCRIPTION"),
                        ItemStatus.fromLong(rs.getLong("ITEM_STATUS")),
                        catDAO.findById(rs.getLong("ITEM_CATEGORY")),
                        rs.getLong("SERVING_SIZE"));
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new DAOException("Error in findById()", e);
        }
    }

    /**
     * Writes dish into database
     * @param dish dish to persist in DB
     * @return this dish itself with id field correctly set
     * @throws DAOException
     */
    public Dish create(Dish dish) throws DAOException {
        String generated[] = {"ITEM_ID"};
        try (PreparedStatement ps = connection.prepareStatement(INSERT_DISH, generated)) {
            ps.setString(1, dish.getName());
            ps.setLong(2, dish.getPrice());
            ps.setString(3, dish.getDescription() != null ? dish.getDescription() : "");
            ps.setLong(4, dish.getItemStatus().getId());
            ps.setLong(5, dish.getCategory().getId());
            ps.setLong(6, dish.getServingSize());
            ps.executeUpdate();
            ResultSet generatedKeys = ps.getGeneratedKeys();
            if (generatedKeys.next()) {
                dish.setId(generatedKeys.getLong(1));
            } else {
                throw new DAOException("No ID obtained");
            }
            return dish;
        } catch (SQLException e) {
            throw new DAOException("Error in create()", e);
        }
    }

    /**
     * Updates dish in database
     * @param dish dish to update in DB
     * @throws DAOException
     */
    public void update(Dish dish) throws DAOException {
        try (PreparedStatement ps = connection.prepareStatement(UPDATE_DISH)) {
            ps.setString(1, dish.getName());
            ps.setLong(2, dish.getPrice());
            ps.setString(3, dish.getDescription() != null ? dish.getDescription() : "");
            ps.setLong(4, dish.getItemStatus().getId());
            ps.setLong(5, dish.getCategory().getId());
            ps.setLong(6, dish.getServingSize());
            ps.setLong(7, dish.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Error in update()", e);
        }
    }
}
