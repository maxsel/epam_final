package by.epam.restaurant.dao;

/**
 * Exception which is thrown when any problems with working with database occur
 */
public class DAOException extends Exception {
    private static final long serialVersionUID = 1243369406657079176L;

    DAOException() {
    }

    DAOException(String message) {
        super(message);
    }

    DAOException(String message, Throwable cause) {
        super(message, cause);
    }

    DAOException(Throwable cause) {
        super(cause);
    }

    DAOException(String message,
                        Throwable cause,
                        boolean enableSuppression,
                        boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
