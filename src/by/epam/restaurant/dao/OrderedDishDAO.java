package by.epam.restaurant.dao;

import by.epam.restaurant.entity.Order;
import by.epam.restaurant.entity.OrderedDish;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Data Access Object for writing {@link OrderedDish}'s from/into database
 */
public class OrderedDishDAO extends AbstractDAO {
    private static final String INSERT_ORDERED_DISH =
            "INSERT INTO ITEMS_TO_ORDERS (ORDER_ID, ITEM_ID, AMOUNT, INSTANT_BILL) VALUES (?, ?, ?, ?)";
    private static final String SELECT_BY_ORDER_ID =
            "SELECT ORDER_ID, ITEM_ID, AMOUNT, INSTANT_BILL FROM ITEMS_TO_ORDERS WHERE ORDER_ID=?";
    private static final String DELETE_BY_ORDER =
            "DELETE FROM ITEMS_TO_ORDERS WHERE ORDER_ID=?";

    public OrderedDishDAO() throws DAOException {}

    /**
     * Writes OrderedDish into database
     * @param dish OrderedDish to persist in DB
     * @return this OrderedDish itself with id field correctly set
     * @throws DAOException
     */
    public OrderedDish create(OrderedDish dish)
            throws DAOException {
        try (PreparedStatement ps = connection.prepareStatement(INSERT_ORDERED_DISH)) {
            ps.setLong(1, dish.getOrder() != null ? dish.getOrder().getId() : 0);
            ps.setLong(2, dish.getDish() != null ? dish.getDish().getId() : 0);
            ps.setLong(3, dish.getAmount());
            ps.setLong(4, dish.getInstantBill());
            ps.executeUpdate();
            return dish;
        } catch (SQLException e) {
            throw new DAOException("Error in create()", e);
        }
    }

    /**
     * Finds all OrderedDishes belonging to concrete order
     * @param order order which OrderedDishes we want to obtain
     * @return List of OrderedDishes with all OrderedDishes of that order
     * @throws DAOException
     */
    public List<OrderedDish> findAllOfOrder(Order order) throws DAOException {
        try (PreparedStatement ps = connection.prepareStatement(SELECT_BY_ORDER_ID);
             OrderDAO orderDAO = new OrderDAO();
             DishDAO dishDAO = new DishDAO()) {
            ps.setLong(1, order.getId());
            ResultSet rs = ps.executeQuery();
            List<OrderedDish> dishes = new ArrayList<>();
            while (rs.next()) {
                OrderedDish dish = new OrderedDish();
                dish.setOrder(orderDAO.findById(rs.getLong("ORDER_ID")));
                dish.setDish(dishDAO.findById(rs.getLong("ITEM_ID")));
                dish.setAmount(rs.getLong("AMOUNT"));
                dish.setInstantBill(rs.getLong("INSTANT_BILL"));
                dishes.add(dish);
            }
            return dishes;
        } catch (SQLException e) {
            throw new DAOException("Error in findAllOfOrder()", e);
        }
    }

    /**
     * Deletes all OrderedDishes belonging to concrete order
     * @param order order which OrderedDishes we want to delete
     * @throws DAOException
     */
    public void deleteAllOfOrder(Order order) throws DAOException {
        try (PreparedStatement ps = connection.prepareStatement(DELETE_BY_ORDER)) {
            ps.setLong(1, order.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Error in deleteAllOfOrder()", e);
        }
    }
}
