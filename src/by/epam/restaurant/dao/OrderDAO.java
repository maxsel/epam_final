package by.epam.restaurant.dao;

import by.epam.restaurant.entity.Bill;
import by.epam.restaurant.entity.Order;
import by.epam.restaurant.entity.OrderStatus;
import by.epam.restaurant.entity.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Data Access Object for writing {@link Order}'s from/into database
 */
public class OrderDAO extends AbstractDAO {
    private static final String INSERT_ORDER =
            "INSERT INTO ORDERS (CLIENT_ID, STATUS_ID, ORDER_DATE)" +
                    " VALUES (?, ?, ?)";
    private static final String SELECT_BY_USER =
            "SELECT ORDER_ID, CLIENT_ID, STATUS_ID, ORDER_DATE, CONFIRM_DATE, BILL_ID " +
                    "FROM ORDERS WHERE CLIENT_ID=? ORDER BY ORDER_ID";
    private static final String SELECT_BY_BILL =
            "SELECT ORDER_ID, CLIENT_ID, STATUS_ID, ORDER_DATE, CONFIRM_DATE, BILL_ID " +
                    "FROM ORDERS WHERE BILL_ID=? ORDER BY ORDER_ID";
    private static final String SELECT_BY_USER_STATUS =
            "SELECT ORDER_ID, CLIENT_ID, STATUS_ID, ORDER_DATE, CONFIRM_DATE, BILL_ID " +
                    "FROM ORDERS WHERE CLIENT_ID=? AND STATUS_ID=? ORDER BY ORDER_ID";
    private static final String SELECT_BY_ID =
            "SELECT ORDER_ID, CLIENT_ID, STATUS_ID, ORDER_DATE, CONFIRM_DATE, BILL_ID " +
                    "FROM ORDERS WHERE ORDER_ID=?";
    private static final String SELECT_ALL =
            "SELECT ORDER_ID, CLIENT_ID, STATUS_ID, ORDER_DATE, CONFIRM_DATE, BILL_ID " +
                    "FROM ORDERS ORDER BY ORDER_ID";
    private static final String SET_STATUS_CONFIRMED =
            "UPDATE ORDERS SET STATUS_ID=?, CONFIRM_DATE=? WHERE ORDER_ID=?";
    private static final String UPDATE = "UPDATE ORDERS " +
            "SET CLIENT_ID=?, STATUS_ID=?, ORDER_DATE=?, CONFIRM_DATE=?, BILL_ID=? " +
            "WHERE ORDER_ID=?";
    private static final String DELETE_BY_ID = "DELETE FROM ORDERS WHERE ORDER_ID=?";

    public OrderDAO() throws DAOException {
    }

    /**
     * Writes order into database
     * @param order order to persist in DB
     * @return this order itself with id field correctly set
     * @throws DAOException
     */
    public Order create(Order order) throws DAOException {
        String generated[] = {"ORDER_ID"};
        try (PreparedStatement ps =
                     connection.prepareStatement(INSERT_ORDER, generated)) {
            ps.setLong(1, order.getClient() == null
                                    ? 0 : order.getClient().getId());
            ps.setLong(2, order.getOrderStatus() == null
                                    ? 0 : order.getOrderStatus().getId());
            ps.setTimestamp(3, order.getOrderDate() == null
                                    ? null : Timestamp.valueOf(order.getOrderDate()));
            ps.executeUpdate();
            ResultSet generatedKeys = ps.getGeneratedKeys();
            if (generatedKeys.next()) {
                order.setId(generatedKeys.getLong(1));
            } else {
                throw new DAOException("No ID obtained");
            }
            return order;
        } catch (SQLException e) {
            throw new DAOException("Error in create()", e);
        }
    }

    /**
     * Finds order with given id.
     * @param id id of order to look for
     * @return order with given id or null if it doesn't exist
     * @throws DAOException
     */
    public Order findById(long id) throws DAOException {
        try (PreparedStatement ps = connection.prepareStatement(SELECT_BY_ID);
             UserDAO userDAO = new UserDAO();
             BillDAO billDAO = new BillDAO()) {
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return makeOrder(rs, userDAO, billDAO);
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new DAOException("Error in findById()", e);
        }
    }


    /**
     * Finds all order created by concrete {@link User}
     * and with given {@link OrderStatus}
     * @param client user whose bills we are looking for
     * @param status status which bills should have
     * @return List of Orders with all orders of that user with given status
     * @throws DAOException
     */
    public List<Order> findByUserStatus(User client, OrderStatus status)
            throws DAOException {
        try (PreparedStatement ps =
                     connection.prepareStatement(SELECT_BY_USER_STATUS);
             UserDAO userDAO = new UserDAO();
             BillDAO billDAO = new BillDAO()) {
            ps.setLong(1, client.getId());
            ps.setLong(2, status.getId());
            return fetchResultSet(ps, userDAO, billDAO);
        } catch (SQLException e) {
            throw new DAOException("Error in findByUserStatus()", e);
        }
    }

    /**
     * Finds all order created by concrete {@link User}
     * @param client user whose bills we are looking for
     * @return List of Orders with all orders of that user
     * @throws DAOException
     */
    public List<Order> findAllOfUser(User client) throws DAOException {
        try (PreparedStatement ps = connection.prepareStatement(SELECT_BY_USER);
             UserDAO userDAO = new UserDAO();
             BillDAO billDAO = new BillDAO()) {
            ps.setLong(1, client.getId());
            return fetchResultSet(ps, userDAO, billDAO);
        } catch (SQLException e) {
            throw new DAOException("Error in findAllOfUser()", e);
        }
    }

    /**
     * Finds all order belonging to concrete bill
     * @param bill bill which orders we want to obtain
     * @return List of Orders with all orders of that bill
     * @throws DAOException
     */
    public List<Order> findAllOfBill(Bill bill) throws DAOException {
        try (PreparedStatement ps = connection.prepareStatement(SELECT_BY_BILL);
             UserDAO userDAO = new UserDAO();
             BillDAO billDAO = new BillDAO()) {
            ps.setLong(1, bill.getId());
            return fetchResultSet(ps, userDAO, billDAO);
        } catch (SQLException e) {
            throw new DAOException("Error in findAllOfBill()", e);
        }
    }

    /**
     * Finds all orders
     * @return List of all orders in DB
     * @throws DAOException
     */
    public List<Order> findAll() throws DAOException {
        try (PreparedStatement ps = connection.prepareStatement(SELECT_ALL);
             UserDAO userDAO = new UserDAO();
             BillDAO billDAO = new BillDAO()) {
            return fetchResultSet(ps, userDAO, billDAO);
        } catch (SQLException e) {
            throw new DAOException("Error in findAll()", e);
        }
    }

    /**
     * Sets order's status to CONFIRMED, updating confirmDateTime accordingly
     * @param id order's id
     * @param confirmDateTime time of order's confirmation
     * @throws DAOException
     */
    public void confirmById(long id, LocalDateTime confirmDateTime)
            throws DAOException {
        try (PreparedStatement ps =
                     connection.prepareStatement(SET_STATUS_CONFIRMED)) {
            ps.setLong(1, OrderStatus.CONFIRMED.getId());
            ps.setTimestamp(2, Timestamp.valueOf(confirmDateTime));
            ps.setLong(3, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Error in confirmById()", e);
        }
    }

    /**
     * Updates order in database
     * @param order dish to update in DB
     * @return updated order
     * @throws DAOException
     */
    public Order update(Order order) throws DAOException {
        try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
            ps.setLong(1, order.getClient() != null ? order.getClient().getId() : 0);
            ps.setLong(2, order.getOrderStatus() != null
                                ? order.getOrderStatus().getId() : 1);
            ps.setTimestamp(3, order.getOrderDate() != null
                                ? Timestamp.valueOf(order.getOrderDate()) : null);
            ps.setTimestamp(4, order.getConfirmDate() != null
                                ? Timestamp.valueOf(order.getConfirmDate()) : null);
            ps.setLong(5, order.getBill() != null ? order.getBill().getId() : 0);
            ps.setLong(6, order.getId());
            ps.executeUpdate();
            return order;
        } catch (SQLException e) {
            throw new DAOException("Error in update()", e);
        }
    }

    /**
     * Deletes order with given id
     * @param order order to delete
     * @throws DAOException
     */
    public void delete(Order order) throws DAOException {
        try (PreparedStatement ps = connection.prepareStatement(DELETE_BY_ID)) {
            ps.setLong(1, order.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Error in delete()", e);
        }
    }

    private Order makeOrder(ResultSet rs, UserDAO userDAO, BillDAO billDAO)
            throws SQLException, DAOException {
        Order order = new Order();
        order.setId(rs.getLong("ORDER_ID"));
        order.setClient(userDAO.findById(rs.getLong("CLIENT_ID")));
        order.setOrderStatus(OrderStatus.fromLong(rs.getLong("STATUS_ID")));
        order.setOrderDate(rs.getTimestamp("ORDER_DATE").toLocalDateTime());
        Timestamp confirmDate = rs.getTimestamp("CONFIRM_DATE");
        if (confirmDate != null) {
            order.setConfirmDate(confirmDate.toLocalDateTime());
        }
        long billId = rs.getLong("BILL_ID");
        if (billId != 0) {
            order.setBill(billDAO.findById(billId));
        }
        return order;
    }


    private List<Order> fetchResultSet(PreparedStatement ps,
                                       UserDAO userDAO, BillDAO billDAO)
            throws SQLException, DAOException {
        ResultSet rs = ps.executeQuery();
        List<Order> orders = new ArrayList<>();
        while (rs.next()) {
            Order order = makeOrder(rs, userDAO, billDAO);
            orders.add(order);
        }
        return orders;
    }
}
