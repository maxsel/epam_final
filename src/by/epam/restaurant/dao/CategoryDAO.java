package by.epam.restaurant.dao;

import by.epam.restaurant.entity.Category;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Data Access Object for writing {@link Category}'s from/into database
 */
public class CategoryDAO extends AbstractDAO {
    private static final String SELECT_ALL = "SELECT * FROM CATEGORIES";
    private static final String SELECT_BY_ID = "SELECT * FROM CATEGORIES WHERE CATEGORY_ID=?";

    public CategoryDAO() throws DAOException {
    }

    /**
     * Retrieves all categories from database
     * @return List with all categories
     * @throws DAOException
     */
    public List<Category> findAll() throws DAOException {
        try (PreparedStatement ps = connection.prepareStatement(SELECT_ALL)) {
            List<Category> categories = new ArrayList<>();
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Category c = new Category(rs.getLong("category_id"), rs.getString("name"));
                categories.add(c);
            }
            return categories;
        } catch (SQLException e) {
            throw new DAOException("Error in findAll()", e);
        }
    }

    /**
     * Finds category with given id.
     * @param id id of category to look for
     * @return category with given id or null if it doesn't exist
     * @throws DAOException
     */
    public Category findById(Long id) throws DAOException {
        try (PreparedStatement ps = connection.prepareStatement(SELECT_BY_ID)) {
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return new Category(rs.getLong("CATEGORY_ID"), rs.getString("NAME"));
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new DAOException("Error in findById()", e);
        }
    }
}
