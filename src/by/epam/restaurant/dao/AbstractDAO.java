package by.epam.restaurant.dao;

import by.epam.restaurant.connectionpool.ConnectionPool;
import by.epam.restaurant.connectionpool.ConnectionPoolException;
import by.epam.restaurant.connectionpool.ProxyConnection;

/**
 * Abstract class for all DAO's, implementing {@link AutoCloseable}
 */
abstract class AbstractDAO implements AutoCloseable {
    ProxyConnection connection;

    /**
     * Gets new connection from {@link ConnectionPool}
     * @throws DAOException
     */
    AbstractDAO() throws DAOException {
        try {
            connection = ConnectionPool.getInstance().getConnection();
        } catch (ConnectionPoolException e) {
            throw new DAOException(e);
        }
    }

    /**
     * Returns DAO's connection to pool.
     */
    @Override
    public void close() {
        ConnectionPool.getInstance().releaseConnection(connection);
    }
}