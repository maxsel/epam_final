package by.epam.restaurant.dao;

import by.epam.restaurant.entity.Bill;
import by.epam.restaurant.entity.BillStatus;
import by.epam.restaurant.entity.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Data Access Object for writing {@link Bill}'s from/into database
 */
public class BillDAO extends AbstractDAO {
    private static final String INSERT_BILL =
            "INSERT INTO BILLS(PAY_DATE, CONFIRM_DATE, STATUS_ID, TIP, CLIENT_ID, CREATION_DATE) " +
            "VALUES (?, ?, ?, ?, ?, ?)";
    private static final String SELECT_ALL =
            "SELECT BILL_ID, PAY_DATE, CONFIRM_DATE, STATUS_ID, TIP, CLIENT_ID, CREATION_DATE " +
            "FROM BILLS";
    private static final String SELECT_BY_ID =
            "SELECT BILL_ID, PAY_DATE, CONFIRM_DATE, STATUS_ID, TIP, CLIENT_ID, CREATION_DATE " +
            "FROM BILLS WHERE BILL_ID=?";
    private static final String SELECT_BY_USER =
            "SELECT BILL_ID, PAY_DATE, TIP, CONFIRM_DATE, STATUS_ID, CLIENT_ID, CREATION_DATE " +
            "FROM BILLS WHERE CLIENT_ID=?";
    private static final String SET_STATUS_CONFIRMED =
            "UPDATE BILLS SET STATUS_ID=?, CONFIRM_DATE=? WHERE BILL_ID=?";
    private static final String SET_STATUS_PAID =
            "UPDATE BILLS SET STATUS_ID=?, PAY_DATE=? WHERE BILL_ID=?";

    public BillDAO() throws DAOException {}

    /**
     * Finds bill with given id.
     * @param id id of bill to look for
     * @return bill with given id or null if it doesn't exist
     * @throws DAOException
     */
    public Bill findById(long id) throws DAOException {
        try (PreparedStatement ps = connection.prepareStatement(SELECT_BY_ID);
             UserDAO userDAO = new UserDAO()) {
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return makeBill(rs, userDAO);
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new DAOException("Error in findById()", e);
        }
    }

    /**
     * Writes bill into database
     * @param bill bill to persist in DB
     * @return this bill itself with id field correctly set
     * @throws DAOException
     */
    public Bill create(Bill bill) throws DAOException {
        String generated[] = {"BILL_ID"};
        try (PreparedStatement ps =
                     connection.prepareStatement(INSERT_BILL, generated)) {
            ps.setTimestamp(1, bill.getPayDate() == null
                                ? null : Timestamp.valueOf(bill.getPayDate()));
            ps.setTimestamp(2, bill.getConfirmDate() == null
                                ? null : Timestamp.valueOf(bill.getConfirmDate()));
            ps.setTimestamp(6, bill.getCreationDate() == null
                                ? null : Timestamp.valueOf(bill.getCreationDate()));
            ps.setLong(3, bill.getStatus().getId());
            ps.setLong(4, bill.getTip());
            ps.setLong(5, bill.getClient() == null ? 0 : bill.getClient().getId());
            ps.executeQuery();
            ResultSet generatedKeys = ps.getGeneratedKeys();
            if (generatedKeys.next()) {
                bill.setId(generatedKeys.getLong(1));
            } else {
                throw new DAOException("No ID obtained");
            }
            return bill;
        } catch (SQLException e) {
            throw new DAOException("Error in create()", e);
        }
    }

    /**
     * Finds all bills created by concrete {@link User}
     * @param client user whose bills we are looking for
     * @return List of Bills with all bills of that user
     * @throws DAOException
     */
    public List<Bill> findAllOfUser(User client) throws DAOException {
        try (PreparedStatement ps = connection.prepareStatement(SELECT_BY_USER);
             UserDAO userDAO = new UserDAO()) {
            ps.setLong(1, client.getId());
            ResultSet rs = ps.executeQuery();
            List<Bill> bills = new ArrayList<>();
            while (rs.next()) {
                Bill bill = makeBill(rs, userDAO);
                bills.add(bill);
            }
            return bills;
        } catch (SQLException e) {
            throw new DAOException("Error in findAllOfUser()", e);
        }
    }

    /**
     * Finds all bills
     * @return List of all bills in DB
     * @throws DAOException
     */
    public List<Bill> findAll() throws DAOException {
        try (PreparedStatement ps = connection.prepareStatement(SELECT_ALL);
             UserDAO userDAO = new UserDAO()) {
            ResultSet rs = ps.executeQuery();
            List<Bill> bills = new ArrayList<>();
            while (rs.next()) {
                Bill bill = makeBill(rs, userDAO);
                bills.add(bill);
            }
            return bills;
        } catch (SQLException e) {
            throw new DAOException("Error in findAll()", e);
        }
    }

    /**
     * Sets bills's status to CONFIRMED, updating confirmDateTime accordingly
     * @param id bill's id
     * @param confirmDateTime time of bill's confirmation
     * @throws DAOException
     */
    public void confirmById(long id, LocalDateTime confirmDateTime)
            throws DAOException {
        try (PreparedStatement ps =
                     connection.prepareStatement(SET_STATUS_CONFIRMED)) {
            ps.setLong(1, BillStatus.CONFIRMED.getId());
            ps.setTimestamp(2, Timestamp.valueOf(confirmDateTime));
            ps.setLong(3, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Error in confirmById()", e);
        }
    }

    /**
     * Sets bills's status to PAID, updating payDateTime accordingly
     * @param id bill's id
     * @param payDateTime time when bill was paid
     * @throws DAOException
     */
    public void payById(long id, LocalDateTime payDateTime)
            throws DAOException {
        try (PreparedStatement ps =
                     connection.prepareStatement(SET_STATUS_PAID)) {
            ps.setLong(1, BillStatus.PAID.getId());
            ps.setTimestamp(2, Timestamp.valueOf(payDateTime));
            ps.setLong(3, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Error in payById()", e);
        }
    }

    private Bill makeBill(ResultSet rs, UserDAO userDAO)
            throws SQLException, DAOException {
        Bill bill = new Bill();
        bill.setId(rs.getLong("BILL_ID"));
        bill.setStatus(BillStatus.fromLong(rs.getLong("STATUS_ID")));
        bill.setTip(rs.getLong("TIP"));
        bill.setClient(userDAO.findById(rs.getLong("CLIENT_ID")));
        Timestamp payDate = rs.getTimestamp("PAY_DATE");
        if (payDate != null)
            bill.setPayDate(payDate.toLocalDateTime());
        Timestamp confirmDate = rs.getTimestamp("CONFIRM_DATE");
        if (confirmDate != null)
            bill.setConfirmDate(confirmDate.toLocalDateTime());
        Timestamp creationDate = rs.getTimestamp("CREATION_DATE");
        if (creationDate != null)
            bill.setCreationDate(creationDate.toLocalDateTime());
        return bill;
    }
}
