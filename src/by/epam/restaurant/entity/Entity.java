package by.epam.restaurant.entity;

import java.io.Serializable;

/**
 * Abstract class for all system's entities
 */
abstract class Entity implements Serializable {
    private static final long serialVersionUID = 3460479334045278347L;
    protected long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}