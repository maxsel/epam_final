package by.epam.restaurant.entity;

/**
 * Entity which represents dish
 */
public class Dish extends Entity {
    private static final long serialVersionUID = 5618703588629402022L;

    private String name;
    private long price;
    private String description;
    private ItemStatus itemStatus;
    private Category category;
    private long servingSize;

    public Dish() {
    }

    public Dish(long id, String name, long price, String description,
                ItemStatus itemStatus, Category category, long servingSize) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.description = description;
        this.itemStatus = itemStatus;
        this.category = category;
        this.servingSize = servingSize;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ItemStatus getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(ItemStatus itemStatus) {
        this.itemStatus = itemStatus;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public long getServingSize() {
        return servingSize;
    }

    public void setServingSize(long servingSize) {
        this.servingSize = servingSize;
    }
}
