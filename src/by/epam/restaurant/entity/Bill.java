package by.epam.restaurant.entity;

import java.time.LocalDateTime;

/**
 * Entity that represents bill
 */
public class Bill extends Entity {
    private static final long serialVersionUID = 1675910542428944151L;
    private BillStatus status;
    private User client;
    private LocalDateTime creationDate;
    private LocalDateTime confirmDate;
    private LocalDateTime payDate;
    private long tip;

    public Bill() {}

    public Bill(long id, BillStatus status, User client,
                LocalDateTime creationDate, LocalDateTime confirmDate,
                LocalDateTime payDate, long tip) {
        this.id = id;
        this.payDate = payDate;
        this.confirmDate = confirmDate;
        this.status = status;
        this.tip = tip;
        this.client = client;
        this.creationDate = creationDate;
    }

    public LocalDateTime getPayDate() {
        return payDate;
    }

    public void setPayDate(LocalDateTime payDate) {
        this.payDate = payDate;
    }

    public LocalDateTime getConfirmDate() {
        return confirmDate;
    }

    public void setConfirmDate(LocalDateTime confirmDate) {
        this.confirmDate = confirmDate;
    }

    public BillStatus getStatus() {
        return status;
    }

    public void setStatus(BillStatus status) {
        this.status = status;
    }

    public long getTip() {
        return tip;
    }

    public void setTip(long tip) {
        this.tip = tip;
    }

    public User getClient() {
        return client;
    }

    public void setClient(User client) {
        this.client = client;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Bill bill = (Bill) o;

        if (tip != bill.tip) return false;
        if (id != bill.id) return false;
        if (payDate != null ? !payDate.equals(bill.payDate) : bill.payDate != null)
            return false;
        if (confirmDate != null ? !confirmDate.equals(bill.confirmDate) : bill.confirmDate != null)
            return false;
        if (status != bill.status) return false;
        if (client != null ? !client.equals(bill.client) : bill.client != null)
            return false;
        return creationDate != null ? creationDate.equals(bill.creationDate) : bill.creationDate == null;

    }

    @Override
    public int hashCode() {
        int result = payDate != null ? payDate.hashCode() : 0;
        result = 31 * result + (confirmDate != null ? confirmDate.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (int) (tip ^ (tip >>> 32));
        result = 31 * result + (client != null ? client.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        result = 31 * result + (int) (id ^ (id >>> 32));
        return result;
    }
}
