package by.epam.restaurant.entity;

/**
 * Enum with possible bill's statuses
 */
public enum BillStatus {
    NEW(1),
    CONFIRMED(2),
    PAID(3);

    private long id;

    BillStatus(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public static BillStatus fromLong(long i) {
        BillStatus status;
        switch ((int)i) {
            case 1:
                status = BillStatus.NEW;
                break;
            case 2:
                status = BillStatus.CONFIRMED;
                break;
            case 3:
                status = BillStatus.PAID;
                break;
            default:
                status = BillStatus.NEW;
                break;
        }
        return status;
    }
}
