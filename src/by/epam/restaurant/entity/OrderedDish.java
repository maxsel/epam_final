package by.epam.restaurant.entity;

/**
 * Entity that represents some amount of ordered dishes in the order
 */
public class OrderedDish {
    private Order order;
    private Dish dish;
    private long amount;
    private long instantBill;

    public OrderedDish() {}

    public OrderedDish(Order order, Dish dish, long amount) {
        this.order = order;
        this.dish = dish;
        this.amount = amount;
    }

    public OrderedDish(Order order, Dish dish, long amount, long instantBill) {
        this.order = order;
        this.dish = dish;
        this.amount = amount;
        this.instantBill = instantBill;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public long getInstantBill() {
        return instantBill;
    }

    public void setInstantBill(long instantBill) {
        this.instantBill = instantBill;
    }
}
