package by.epam.restaurant.entity;

/**
 * Enum with all possible order's statuses
 */
public enum OrderStatus {
    NEW(1),
    CONFIRMED(2),
    PAID(3);

    private long id;

    OrderStatus(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public static OrderStatus fromLong(long i) {
        OrderStatus status;
        switch ((int)i) {
            case 1:
                status = OrderStatus.NEW;
                break;
            case 2:
                status = OrderStatus.CONFIRMED;
                break;
            case 3:
                status = OrderStatus.PAID;
                break;
            default:
                status = OrderStatus.NEW;
                break;
        }
        return status;
    }
}
