package by.epam.restaurant.entity;

/**
 * Entity which represents dish category
 */
public class Category extends Entity {
    private static final long serialVersionUID = -8551142528868011258L;

    private String name;

    public Category() {
    }

    public Category(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
