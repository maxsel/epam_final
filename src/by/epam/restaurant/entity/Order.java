package by.epam.restaurant.entity;

import java.time.LocalDateTime;

/**
 * Entity that represents order
 */
public class Order extends Entity {
    private static final long serialVersionUID = 6002029251595840433L;
    private User client;
    private Bill bill;
    private OrderStatus orderStatus;
    private LocalDateTime orderDate;
    private LocalDateTime confirmDate;

    public Order() {}

    public Order(long id, User client, Bill bill, OrderStatus orderStatus,
                 LocalDateTime orderDate, LocalDateTime confirmDate) {
        this.id = id;
        this.orderDate = orderDate;
        this.orderStatus = orderStatus;
        this.client = client;
        this.confirmDate = confirmDate;
        this.bill = bill;
    }

    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDateTime orderDate) {
        this.orderDate = orderDate;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public User getClient() {
        return client;
    }

    public void setClient(User client) {
        this.client = client;
    }

    public LocalDateTime getConfirmDate() {
        return confirmDate;
    }

    public void setConfirmDate(LocalDateTime confirmDate) {
        this.confirmDate = confirmDate;
    }

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        if (id != order.id) return false;
        if (orderDate != null ? !orderDate.equals(order.orderDate) : order.orderDate != null)
            return false;
        if (orderStatus != order.orderStatus) return false;
        if (client != null ? !client.equals(order.client) : order.client != null)
            return false;
        if (confirmDate != null ? !confirmDate.equals(order.confirmDate) : order.confirmDate != null)
            return false;
        return bill != null ? bill.equals(order.bill) : order.bill == null;

    }

    @Override
    public int hashCode() {
        int result = orderDate != null ? orderDate.hashCode() : 0;
        result = 31 * result + (orderStatus != null ? orderStatus.hashCode() : 0);
        result = 31 * result + (client != null ? client.hashCode() : 0);
        result = 31 * result + (confirmDate != null ? confirmDate.hashCode() : 0);
        result = 31 * result + (bill != null ? bill.hashCode() : 0);
        result = 31 * result + (int) (id ^ (id >>> 32));
        return result;
    }
}
