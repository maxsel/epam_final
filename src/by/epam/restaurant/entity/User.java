package by.epam.restaurant.entity;

/**
 * Entity which represents user
 */
public class User extends Entity {
    private static final long serialVersionUID = 87738175875212836L;

    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private UserRole role;
    private long balance;

    public User() {
    }

    public User(long id, String login, String password, String firstName,
                String lastName, UserRole role, long balance) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.role = role;
        this.balance = balance;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }
}