package by.epam.restaurant.entity;

/**
 * Enum with possible dish's statuses
 */
public enum ItemStatus {
    PRESENT(1),
    NOT_PRESENT(2);

    private long id;

    ItemStatus(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public static ItemStatus fromLong(long i) {
        ItemStatus status;
        switch ((int)i) {
            case 1:
                status = ItemStatus.PRESENT;
                break;
            case 2:
            default:
                status = ItemStatus.NOT_PRESENT;
                break;
        }
        return status;
    }
}
