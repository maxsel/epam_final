package by.epam.restaurant.listener;

import by.epam.restaurant.connectionpool.ConnectionPool;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Listener that manages {@link ConnectionPool's} init and release.
 */
@WebListener
public class ConnectionPoolInitListener implements ServletContextListener {
    /**
     * Initializes connection pool at the start of application.
     * @param servletContextEvent contextInitialized
     */
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ConnectionPool.getInstance();
    }

    /**
     * Releases connection pool when application is finishing.
     * @param servletContextEvent contextDestroyed
     */
    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        ConnectionPool.getInstance().releasePool();
    }
}
