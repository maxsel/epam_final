package by.epam.restaurant.service;

import by.epam.restaurant.dao.BillDAO;
import by.epam.restaurant.dao.DAOException;
import by.epam.restaurant.entity.Bill;
import by.epam.restaurant.entity.User;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Service for working with {@link Bill}s
 */
public class BillService {
    /**
     * Writes bill into database
     * @param bill bill to persist in DB
     * @return this bill itself with id field correctly set
     * @throws ServiceException
     */
    public static Bill create(Bill bill) throws ServiceException {
        try (BillDAO dao = new BillDAO()) {
            return dao.create(bill);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Finds all bills created by concrete {@link User}
     * @param user user whose bills we are looking for
     * @return List of Bills with all bills of that user
     * @throws ServiceException
     */
    public static List<Bill> findAllOfUser(User user) throws ServiceException {
        try (BillDAO dao = new BillDAO()) {
            return dao.findAllOfUser(user);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Finds all bills
     * @return List of all bills in DB
     * @throws ServiceException
     */
    public static List<Bill> findAll() throws ServiceException {
        try (BillDAO dao = new BillDAO()) {
            return dao.findAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Finds bill with given id.
     * @param id id of bill to look for
     * @return bill with given id or null if it doesn't exist
     * @throws ServiceException
     */
    public static Bill findById(long id) throws ServiceException {
        try (BillDAO dao = new BillDAO()) {
            return dao.findById(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Sets bills's status to CONFIRMED, updating confirmDateTime accordingly
     * @param id bill's id
     * @param confirmDateTime time of bill's confirmation
     * @throws ServiceException
     */
    public static void confirmById(long id, LocalDateTime confirmDateTime)
            throws ServiceException {
        try (BillDAO dao = new BillDAO()) {
            dao.confirmById(id, confirmDateTime);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Sets bills's status to PAID, updating payDateTime accordingly
     * @param id bill's id
     * @param payDateTime time when bill was paid
     * @throws ServiceException
     */
    public static void payById(long id, LocalDateTime payDateTime)
            throws ServiceException {
        try (BillDAO dao = new BillDAO()) {
            dao.payById(id, payDateTime);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
