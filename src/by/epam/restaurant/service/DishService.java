package by.epam.restaurant.service;

import by.epam.restaurant.dao.DAOException;
import by.epam.restaurant.dao.DishDAO;
import by.epam.restaurant.entity.Dish;

import java.util.List;

/**
 * Service for working with {@link Dish}s
 */
public class DishService {
    /**
     * Retrieves all dishes from database
     * @return List with all dishes
     * @throws ServiceException
     */
    public static List<Dish> findAll() throws ServiceException {
        try (DishDAO dao = new DishDAO()) {
            return dao.findAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Finds dish with given id.
     * @param id id of dish to look for
     * @return dish with given id or null if it doesn't exist
     * @throws ServiceException
     */
    public static Dish findById(long id) throws ServiceException {
        try (DishDAO dao = new DishDAO()) {
            return dao.findById(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Writes dish into database
     * @param dish dish to persist in DB
     * @return this dish itself with id field correctly set
     * @throws ServiceException
     */
    public static Dish create(Dish dish) throws ServiceException {
        try (DishDAO dao = new DishDAO()) {
            return dao.create(dish);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Updates dish in database
     * @param dish dish to update in DB
     * @throws ServiceException
     */
    public static void update(Dish dish) throws ServiceException {
        try (DishDAO dao = new DishDAO()) {
            dao.update(dish);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
