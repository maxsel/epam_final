package by.epam.restaurant.service;

/**
 * Exception which is thrown when any problems with working with entity occur
 */
public class ServiceException extends Exception {
    private static final long serialVersionUID = -3260883758601804148L;

    ServiceException() {
    }

    ServiceException(String message) {
        super(message);
    }

    ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    ServiceException(Throwable cause) {
        super(cause);
    }

    ServiceException(String message,
                     Throwable cause,
                     boolean enableSuppression,
                     boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
