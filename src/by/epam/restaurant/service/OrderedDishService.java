package by.epam.restaurant.service;

import by.epam.restaurant.dao.DAOException;
import by.epam.restaurant.dao.OrderedDishDAO;
import by.epam.restaurant.entity.Order;
import by.epam.restaurant.entity.OrderedDish;

import java.util.List;

/**
 * Service for working with {@link OrderedDish}
 */
public class OrderedDishService {
    /**
     * Writes OrderedDish into database
     * @param dish OrderedDish to persist in DB
     * @return this OrderedDish itself with id field correctly set
     * @throws ServiceException
     */
    public static OrderedDish create(OrderedDish dish)
            throws ServiceException {
        try (OrderedDishDAO dao = new OrderedDishDAO()) {
            return dao.create(dish);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Finds all OrderedDishes belonging to concrete order
     * @param order order which OrderedDishes we want to obtain
     * @return List of OrderedDishes with all OrderedDishes of that order
     * @throws ServiceException
     */
    public static List<OrderedDish> findAllOfOrder(Order order)
            throws ServiceException {
        try (OrderedDishDAO dao = new OrderedDishDAO()) {
            return dao.findAllOfOrder(order);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Deletes all OrderedDishes belonging to concrete order
     * @param order order which OrderedDishes we want to delete
     * @throws ServiceException
     */
    public static void deleteAllOfOrder(Order order) throws ServiceException {
        if (order != null) {
            try (OrderedDishDAO dao = new OrderedDishDAO()) {
                dao.deleteAllOfOrder(order);
            } catch (DAOException e) {
                throw new ServiceException(e);
            }
        }
    }
}
