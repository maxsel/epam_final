package by.epam.restaurant.service;

import by.epam.restaurant.dao.CategoryDAO;
import by.epam.restaurant.dao.DAOException;
import by.epam.restaurant.entity.Category;

import java.util.List;

/**
 * Service for working with {@link Category}s
 */
public class CategoryService {
    /**
     * Retrieves all categories from database
     * @return List with all categories
     * @throws ServiceException
     */
    public static List<Category> findAll() throws ServiceException {
        try (CategoryDAO dao = new CategoryDAO()) {
            return dao.findAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Finds category with given id.
     * @param id id of category to look for
     * @return category with given id or null if it doesn't exist
     * @throws ServiceException
     */
    public static Category findById(long id) throws ServiceException {
        try (CategoryDAO dao = new CategoryDAO()) {
            return dao.findById(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
