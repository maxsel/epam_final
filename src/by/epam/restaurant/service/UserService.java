package by.epam.restaurant.service;

import by.epam.restaurant.dao.DAOException;
import by.epam.restaurant.dao.UserDAO;
import by.epam.restaurant.entity.User;
import by.epam.restaurant.entity.UserRole;

public class UserService {
    private static final int INITIAL_CASH = 1_000_000;

    /**
     * Writes user into database
     * @param user user to persist in DB
     * @return this user itself with id field correctly set
     * @throws ServiceException
     */
    public static User registerNewUser(User user) throws ServiceException {
        try (UserDAO dao = new UserDAO()) {
            user.setRole(UserRole.CLIENT);
            user.setBalance(INITIAL_CASH);
            return dao.registerNewUser(user);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Finds user with given login and password.
     * @param login id of user to look for
     * @param password password of user to look for
     * @return user with given credentials or null if it doesn't exist
     * @throws ServiceException
     */
    public static User findByCredentials(String login, String password)
            throws ServiceException {
        try (UserDAO dao = new UserDAO()) {
            return dao.findByCredentials(login, password);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Defines if user with such login already exists
     * @param login login
     * @return true if user with such login already exists
     * @throws ServiceException
     */
    public static boolean isLoginFree(String login) throws ServiceException {
        try (UserDAO dao = new UserDAO()) {
            return dao.isLoginFree(login);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Updates user in database
     * @param user user to update in DB
     * @throws ServiceException
     */
    public static User update(User user) throws ServiceException {
        try (UserDAO dao = new UserDAO()) {
            return dao.update(user);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Deletes given user from database by id
     * @param user user to deleteAllOfOrder
     * @throws ServiceException
     */
    public static void delete(User user) throws ServiceException {
        try (UserDAO dao = new UserDAO()) {
            dao.delete(user);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
