package by.epam.restaurant.service;

import by.epam.restaurant.dao.DAOException;
import by.epam.restaurant.dao.OrderDAO;
import by.epam.restaurant.entity.Bill;
import by.epam.restaurant.entity.Order;
import by.epam.restaurant.entity.OrderStatus;
import by.epam.restaurant.entity.User;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Service for working with {@link Order}
 */
public class OrderService {
    /**
     * Writes order into database
     * @param order order to persist in DB
     * @return this order itself with id field correctly set
     * @throws ServiceException
     */
    public static Order create(Order order)
            throws ServiceException {
        try (OrderDAO dao = new OrderDAO()) {
            return dao.create(order);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Finds all order created by concrete {@link User}
     * @param client user whose bills we are looking for
     * @return List of Orders with all orders of that user
     * @throws ServiceException
     */
    public static List<Order> findAllOfUser(User client)
            throws ServiceException {
        try (OrderDAO dao = new OrderDAO()) {
            return dao.findAllOfUser(client);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Finds all order belonging to concrete bill
     * @param bill bill which orders we want to obtain
     * @return List of Orders with all orders of that bill
     * @throws ServiceException
     */
    public static List<Order> findAllOfBill(Bill bill)
            throws ServiceException {
        try (OrderDAO dao = new OrderDAO()) {
            return dao.findAllOfBill(bill);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Finds all order created by concrete {@link User} which are confirmed
     * @param client user whose orders we are looking for
     * @return List of Orders with all orders of that user which are confirmed
     * @throws ServiceException
     */
    public static List<Order> findAllConfirmedOfUser(User client)
            throws ServiceException {
        try (OrderDAO dao = new OrderDAO()) {
            return dao.findByUserStatus(client, OrderStatus.CONFIRMED);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Finds all orders
     * @return List of all orders in DB
     * @throws ServiceException
     */
    public static List<Order> findAll() throws ServiceException {
        try (OrderDAO dao = new OrderDAO()) {
            return dao.findAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Finds order with the given id
     * @param id id of order to look for
     * @return order with the given id or null if not exists
     * @throws ServiceException
     */
    public static Order findById(long id) throws ServiceException {
        try (OrderDAO dao = new OrderDAO()) {
            return dao.findById(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Sets order's status to CONFIRMED, updating confirmDateTime accordingly
     * @param id order's id
     * @param confirmDateTime time of order's confirmation
     * @throws ServiceException
     */
    public static void confirmById(long id, LocalDateTime confirmDateTime)
            throws ServiceException {
        try (OrderDAO dao = new OrderDAO()) {
            dao.confirmById(id, confirmDateTime);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Updates order in database
     * @param order dish to update in DB
     * @return updated order
     * @throws ServiceException
     */
    public static Order update(Order order) throws ServiceException {
        try (OrderDAO dao = new OrderDAO()) {
            return dao.update(order);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Deletes order with given id
     * @param order order to delete
     * @throws ServiceException
     */
    public static void delete(Order order) throws ServiceException {
        if (order != null) {
            try (OrderDAO dao = new OrderDAO()) {
                dao.delete(order);
            } catch (DAOException e) {
                throw new ServiceException(e);
            }
        }
    }
}
