package by.epam.restaurant.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * Filter that blocks access directly to {@code .jsp} pages.
 */
@WebFilter(filterName = "PageAccessFilter", urlPatterns = {"/jsp/*"})
public class PageAccessFilter implements Filter {
    private static final String REQUEST_ATTR_MESSAGE = "message";
    private static final String URL_ERROR = "/jsp/error.jsp";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    /**
     * Gives error page as response for requesting any .jsp page directly from browser.
     *
     * @param request     user's request
     * @param response    response of the server
     * @param filterChain {@link FilterChain} of {@link Filter}
     * @throws IOException
     * @throws ServletException if page's address can't be reached
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        request.setAttribute(REQUEST_ATTR_MESSAGE, "Sorry, no page with such URL found");
        request.getServletContext().getRequestDispatcher(URL_ERROR).forward(request, response);
    }

    @Override
    public void destroy() {
    }
}
