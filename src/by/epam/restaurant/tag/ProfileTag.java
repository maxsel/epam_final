package by.epam.restaurant.tag;

import by.epam.restaurant.entity.User;
import by.epam.restaurant.entity.UserRole;
import by.epam.restaurant.util.ResourceManager;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Custom tag, used to display user's profile on webpage
 */
public class ProfileTag extends BodyTagSupport {
    private static final String DEFAULT_LANG = "en_US";
    private static final String PROP_LOGIN = "login";
    private static final String PROP_FIRST_NAME = "first.name";
    private static final String PROP_LAST_NAME = "last.name";
    private static final String PROP_BALANCE = "balance";
    private static final String PROP_ROLE = "role";
    private static final long serialVersionUID = -500606341371687236L;
    private User user;

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int doAfterBody() throws JspException {
        BodyContent content = getBodyContent();
        String lang = content.getString();
        lang = (lang != null && !lang.trim().isEmpty()) ? lang : DEFAULT_LANG;
        String[] loc = lang.split("_");
        ResourceManager manager = ResourceManager.INSTANCE;
        Locale curLocale = new Locale(loc[0],loc[1]);
        manager.changeResource(curLocale);
        StringBuilder fullText = new StringBuilder();
        fullText.append("<div class=\"profile\">")
                .append("<table>")
                .append("<tr>")
                .append("<th>").append(manager.getString(PROP_LOGIN)).append(":</th>")
                .append("<td>").append(user.getLogin()).append("</td>")
                .append("</tr><tr>")
                .append("<th>").append(manager.getString(PROP_FIRST_NAME)).append(":</th>")
                .append("<td>").append(user.getFirstName()).append("</td>")
                .append("</tr><tr>")
                .append("<th>").append(manager.getString(PROP_LAST_NAME)).append(":</th>")
                .append("<td>").append(user.getLastName()).append("</td>")
                .append("</tr>");
        NumberFormat numFormat = NumberFormat.getInstance(curLocale);
        String balance = numFormat.format(user.getBalance());
        fullText.append("<tr><th>").append(manager.getString(PROP_BALANCE)).append(":</th>")
                .append("<td>").append(balance).append("</td></tr>");
        if (user.getRole() == UserRole.ADMINISTRATOR) {
            fullText.append("<tr><th>").append(manager.getString(PROP_ROLE)).append(":</th>")
                    .append("<td>").append(user.getRole()).append("</td></tr>");
        }
        fullText.append("</table></div>");
        JspWriter out = content.getEnclosingWriter();
        try {
            out.write(fullText.toString());
        } catch (IOException e) {
            throw new JspTagException(e.getMessage());
        }
        return SKIP_BODY;
    }
}
