package by.epam.restaurant.tag;

import by.epam.restaurant.util.ResourceManager;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Locale;

/**
 * Custom tag, used to display footer on webpage
 */
public class FooterTag extends TagSupport {
    private static final String SESSION_ATTR_LANG = "lang";
    private static final String DEFAULT_LANG = "en_US";
    private static final String PROP_TO_TOP = "to.top";
    private static final String PROP_FOOTER_MSG = "footer.msg";
    private static final long serialVersionUID = -8556197341924940929L;

    @Override
    public int doStartTag() throws JspTagException {
        String lang = (String) pageContext.getSession().getAttribute(SESSION_ATTR_LANG);
        lang = (lang != null && !lang.trim().isEmpty()) ? lang : DEFAULT_LANG;
        String[] loc = lang.split("_");
        ResourceManager manager = ResourceManager.INSTANCE;
        manager.changeResource(new Locale(loc[0],loc[1]));
        String toTop = manager.getString(PROP_TO_TOP);
        String footerMsg = manager.getString(PROP_FOOTER_MSG);
        String fullText =
                "<p class=\"pull-right\"><a href=\"#\">" + toTop +
                "</a></p><p>" + footerMsg + "</p>";
        try {
            pageContext.getOut().write(fullText);
        } catch (IOException e) {
            throw new JspTagException(e.getMessage());
        }
        return SKIP_BODY;
    }

    @Override
    public int doEndTag() throws JspTagException {
        return EVAL_PAGE;
    }
}
