package by.epam.restaurant.tag;

import by.epam.restaurant.entity.User;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Custom tag, used to display dropdown user's menu on webpage
 */
public class DropdownUserTag extends TagSupport {
    private static final long serialVersionUID = -8165572180047011235L;
    private User user;

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int doStartTag() throws JspTagException {
        String login = user.getLogin();
        String fullText =
                "<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"" +
                "role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">" +
                    "<span class=\"glyphicon glyphicon-user\"/>" +
                    login + "<span class=\"caret\"/>" +
                "</a>";
        try {
            pageContext.getOut().write(fullText);
        } catch (IOException e) {
            throw new JspTagException(e.getMessage());
        }
        return SKIP_BODY;
    }
}
