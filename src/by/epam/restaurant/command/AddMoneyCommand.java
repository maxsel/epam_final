package by.epam.restaurant.command;

import by.epam.restaurant.entity.User;
import by.epam.restaurant.service.ServiceException;
import by.epam.restaurant.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Optional;

/**
 * Command that adds more money to user in session (if exists)
 */
class AddMoneyCommand implements Command {
    private static final String URL_DEFAULT = "/controller?command=empty";
    private static final String URL_PROFILE = "/controller?command=showprofile";
    private static final String USER_SESSION_ATTR = "user";
    private static final int AMOUNT = 100_000;

    /**
     * Adds more money to user in session (if exists)
     * @param request  request from client
     * @param response HttpServletResponse which may be used
     *                 for writing error messages as response to AJAX query
     * @return next page's URL
     * @throws CommandException
     */
    @Override
    public Optional<String> execute(HttpServletRequest request,
                                    HttpServletResponse response)
            throws CommandException {
        try {
            HttpSession session = request.getSession(true);
            User user = (User) session.getAttribute(USER_SESSION_ATTR);
            if (user != null) {
                user.setBalance(user.getBalance() + AMOUNT);
                UserService.update(user);
                session.setAttribute(USER_SESSION_ATTR, user);
                return Optional.of(URL_PROFILE);
            } else {
                return Optional.of(URL_DEFAULT);
            }
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
    }

    @Override
    public boolean needsRedirect() {
        return true;
    }
}
