package by.epam.restaurant.command;

import by.epam.restaurant.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

/**
 * Command that shows login page
 */
class ShowLoginCommand implements Command {
    private static final String URL_DEFAULT = "/controller?command=empty";
    private static final String URL_LOGIN = "/jsp/login.jsp";
    private static final String USER_SESSION_ATTR = "user";

    /**
     * Shows login page
     * @param request  request from client
     * @param response HttpServletResponse which may be used
     *                 for writing error messages as response to AJAX query
     * @return next page's URL
     * @throws CommandException
     */
    @Override
    public Optional<String> execute(HttpServletRequest request,
                                    HttpServletResponse response)
            throws CommandException {
        User user =
                (User) request.getSession(true).getAttribute(USER_SESSION_ATTR);
        if (user != null) {
            return Optional.of(URL_DEFAULT);
        } else {
            return Optional.of(URL_LOGIN);
        }
    }
}
