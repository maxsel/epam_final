package by.epam.restaurant.command;

import by.epam.restaurant.entity.*;
import by.epam.restaurant.service.OrderService;
import by.epam.restaurant.service.OrderedDishService;
import by.epam.restaurant.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Command that makes order from user account
 */
class OrderCommand implements Command {
    private static final String SESSION_ATTR_USER = "user";
    private static final String SESSION_ATTR_ORDERED = "ordered";
    private static final String URL_DEFAULT = "/controller?command=empty";
    private static final String URL_SHOW_NEW_ORDERS =
            "/controller?command=showneworders";

    /**
     * Makes order from user account
     * @param request  request from client
     * @param response HttpServletResponse which may be used
     *                 for writing error messages as response to AJAX query
     * @return next page's URL
     * @throws CommandException
     */
    @Override
    public Optional<String> execute(HttpServletRequest request,
                                    HttpServletResponse response)
            throws CommandException {
        try {
            HttpSession session = request.getSession(true);
            User user = (User) session.getAttribute(SESSION_ATTR_USER);
            if (user == null || user.getRole() != UserRole.CLIENT) {
                return Optional.of(URL_DEFAULT);
            }
            List items = (ArrayList) session.getAttribute(SESSION_ATTR_ORDERED);
            if (items == null) {
                return Optional.of(URL_DEFAULT);
            }
            session.removeAttribute(SESSION_ATTR_ORDERED);
            Order newOrder = new Order();
            newOrder.setClient(user);
            newOrder.setOrderStatus(OrderStatus.NEW);
            newOrder.setOrderDate(LocalDateTime.now());
            newOrder = OrderService.create(newOrder);
            for (Object item : items) {
                OrderedDish dish = (OrderedDish) item;
                dish.setOrder(newOrder);
                dish.setInstantBill(dish.getDish().getPrice()*dish.getAmount());
                OrderedDishService.create(dish);
            }
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return Optional.of(URL_SHOW_NEW_ORDERS);
    }

    @Override
    public boolean needsRedirect() {
        return true;
    }
}
