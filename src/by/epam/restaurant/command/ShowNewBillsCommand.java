package by.epam.restaurant.command;

import by.epam.restaurant.entity.*;
import by.epam.restaurant.service.BillService;
import by.epam.restaurant.service.OrderService;
import by.epam.restaurant.service.OrderedDishService;
import by.epam.restaurant.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Command that shows new bills
 */
class ShowNewBillsCommand implements Command {
    private static final String SESSION_ATTR_USER = "user";
    private static final String REQUEST_ATTR_MAP_BILLS = "mapbills";
    private static final String REQUEST_ATTR_COSTS = "costs";
    private static final String URL_DEFAULT = "/controller?command=empty";
    private static final String URL_NEW_BILLS = "/jsp/new_bills.jsp";

    /**
     * Shows new bills
     * @param request  request from client
     * @param response HttpServletResponse which may be used
     *                 for writing error messages as response to AJAX query
     * @return next page's URL
     * @throws CommandException
     */
    @Override
    public Optional<String> execute(HttpServletRequest request,
                                    HttpServletResponse response)
            throws CommandException {
        try {
            User user =
                    (User) request.getSession(true).getAttribute(SESSION_ATTR_USER);
            if (user == null) {
                return Optional.of(URL_DEFAULT);
            }
            List<Bill> unpaidBills = new ArrayList<>();
            if (user.getRole() == UserRole.CLIENT) {
                unpaidBills = BillService.findAllOfUser(user)
                            .stream()
                            .filter(bill -> bill.getStatus() != BillStatus.PAID)
                            .collect(Collectors.toList());
            }
            if (user.getRole() == UserRole.ADMINISTRATOR) {
                unpaidBills = BillService.findAll();
            }
            Map<Bill, List<OrderedDish>> mapBills = new HashMap<>();
            Map<Long, Long> costs = new HashMap<>();
            for (Bill bill : unpaidBills) {
                List<OrderedDish> allDishes = new LinkedList<>();
                for (Order order : OrderService.findAllOfBill(bill)) {
                    List<OrderedDish> dishes =
                            OrderedDishService.findAllOfOrder(order);
                    allDishes.addAll(dishes);
                }
                mapBills.put(bill, allDishes);
                long cost =
                        allDishes.stream().mapToLong(OrderedDish::getInstantBill).sum();
                costs.put(bill.getId(), cost);
            }
            request.setAttribute(REQUEST_ATTR_MAP_BILLS, mapBills);
            request.setAttribute(REQUEST_ATTR_COSTS, costs);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return Optional.of(URL_NEW_BILLS);
    }
}
