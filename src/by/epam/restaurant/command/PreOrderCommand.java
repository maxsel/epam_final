package by.epam.restaurant.command;

import by.epam.restaurant.entity.Dish;
import by.epam.restaurant.entity.OrderedDish;
import by.epam.restaurant.entity.User;
import by.epam.restaurant.entity.UserRole;
import by.epam.restaurant.service.DishService;
import by.epam.restaurant.service.ServiceException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Command that prepares order from user account
 */
class PreOrderCommand implements Command {
    private static final String MESSAGE_EMPTY_ORDER = "emptyOrder";
    private static final String REQUEST_PARAM_ORDER_JSON = "orderJSON";
    private static final String SESSION_ATTR_ORDERED = "ordered";
    private static final String SESSION_ATTR_USER = "user";
    private static final String URL_DEFAULT = "/controller?command=empty";
    private static final String URL_ORDER = "/controller?command=showorder";

    /**
     * Prepares order from user account
     * @param request  request from client
     * @param response HttpServletResponse which may be used
     *                 for writing error messages as response to AJAX query
     * @return next page's URL
     * @throws CommandException
     */
    @Override
    public Optional<String> execute(HttpServletRequest request,
                                    HttpServletResponse response)
            throws CommandException {
        try {
            String orderJSON = request.getParameter(REQUEST_PARAM_ORDER_JSON);
            HttpSession session = request.getSession(true);
            User user = (User) session.getAttribute(SESSION_ATTR_USER);
            if (user == null || user.getRole() != UserRole.CLIENT) {
                return Optional.of(URL_DEFAULT);
            }
            if (orderJSON != null) {
                Gson gson = new GsonBuilder().create();
                Type collectionType =
                        new TypeToken<List<OrderedDishJSON>>() {}.getType();
                List<OrderedDishJSON> itemsJSON =
                        gson.fromJson(orderJSON, collectionType);
                if (itemsJSON == null || itemsJSON.isEmpty()) {
                    response.getWriter().write(MESSAGE_EMPTY_ORDER);
                    return Optional.empty();
                }
                List<OrderedDish> items = new ArrayList<>();
                for (OrderedDishJSON o : itemsJSON) {
                    Dish dish = DishService.findById(o.id);
                    items.add(new OrderedDish(null, dish, o.amount));
                }
                session.setAttribute(SESSION_ATTR_ORDERED, items);
                response.getWriter().write(URL_ORDER);
            } else {
                response.getWriter().write(MESSAGE_EMPTY_ORDER);
            }
        } catch (ServiceException | IOException e) {
            throw new CommandException(e);
        }
        return Optional.empty();
    }

    private static class OrderedDishJSON {
        private long id;
        private long amount;
    }
}
