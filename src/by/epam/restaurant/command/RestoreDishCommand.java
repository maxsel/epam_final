package by.epam.restaurant.command;

import by.epam.restaurant.entity.*;
import by.epam.restaurant.service.DishService;
import by.epam.restaurant.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Optional;

/**
 * Command that restores dish from admin account
 */
class RestoreDishCommand implements Command {
    private static final String REQUEST_PARAM_ID = "id";
    private static final String SESSION_ATTR_USER = "user";
    private static final String URL_DEFAULT = "/controller?command=empty";
    private static final String URL_MENU = "/controller?command=showmenu";

    /**
     * Restores dish from admin account
     * @param request  request from client
     * @param response HttpServletResponse which may be used
     *                 for writing error messages as response to AJAX query
     * @return next page's URL
     * @throws CommandException
     */
    @Override
    public Optional<String> execute(HttpServletRequest request,
                                    HttpServletResponse response)
            throws CommandException {
        try {
            HttpSession session = request.getSession(true);
            User user = (User) session.getAttribute(SESSION_ATTR_USER);
            if (user == null || user.getRole() != UserRole.ADMINISTRATOR) {
                return Optional.of(URL_DEFAULT);
            }
            long id = Long.parseLong(request.getParameter(REQUEST_PARAM_ID));
            Dish dish = DishService.findById(id);
            dish.setItemStatus(ItemStatus.PRESENT);
            DishService.update(dish);
        } catch (ServiceException | NumberFormatException e) {
            throw new CommandException(e);
        }
        return Optional.of(URL_MENU);
    }
}
