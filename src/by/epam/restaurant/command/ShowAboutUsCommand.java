package by.epam.restaurant.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

/**
 * Command that shows 'about us' page
 */
class ShowAboutUsCommand implements Command {
    private static final String URL_ABOUT_US = "/jsp/about_us.jsp";

    /**
     * Shows 'about us' page
     * @param request  request from client
     * @param response HttpServletResponse which may be used
     *                 for writing error messages as response to AJAX query
     * @return next page's URL
     * @throws CommandException
     */
    @Override
    public Optional<String> execute(HttpServletRequest request,
                                    HttpServletResponse response)
            throws CommandException {
        return Optional.of(URL_ABOUT_US);
    }
}
