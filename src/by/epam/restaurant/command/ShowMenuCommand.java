package by.epam.restaurant.command;

import by.epam.restaurant.entity.Dish;
import by.epam.restaurant.entity.ItemStatus;
import by.epam.restaurant.entity.User;
import by.epam.restaurant.entity.UserRole;
import by.epam.restaurant.service.DishService;
import by.epam.restaurant.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * Command that shows menu
 */
class ShowMenuCommand implements Command {
    private static final String SESSION_ATTR_USER = "user";
    private static final String REQUEST_ATTR_CATEGORIZED = "categorized";
    private static final String URL_MENU = "/jsp/menu.jsp";

    /**
     * Shows menu
     * @param request  request from client
     * @param response HttpServletResponse which may be used
     *                 for writing error messages as response to AJAX query
     * @return next page's URL
     * @throws CommandException
     */
    @Override
    public Optional<String> execute(HttpServletRequest request,
                                    HttpServletResponse response)
            throws CommandException {
        try {
            List<Dish> allDishes = DishService.findAll();
            Map<String, List<Dish>> dishesCategorized;
            TreeMap<String, List<Dish>> categoriesSorted;
            User user =
                    (User) request.getSession(true).getAttribute(SESSION_ATTR_USER);
            if (user == null || user.getRole() != UserRole.ADMINISTRATOR) {
                dishesCategorized =
                        allDishes
                                .stream()
                                .filter(dish -> dish.getItemStatus() == ItemStatus.PRESENT)
                                .collect(Collectors.groupingBy(w -> w.getCategory().getName()));

            } else {
                dishesCategorized =
                        allDishes
                                .stream()
                                .collect(Collectors.groupingBy(w -> w.getCategory().getName()));
            }
            categoriesSorted = new TreeMap<>(dishesCategorized);
            request.setAttribute(REQUEST_ATTR_CATEGORIZED, categoriesSorted);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return Optional.of(URL_MENU);
    }
}
