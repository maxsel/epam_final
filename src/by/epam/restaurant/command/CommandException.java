package by.epam.restaurant.command;

/**
 * Exception which is thrown when any problems with command execution occur
 */
public class CommandException extends Exception {
    private static final long serialVersionUID = 2469956509857175863L;

    public CommandException() {
    }

    public CommandException(String message) {
        super(message);
    }

    public CommandException(String message, Throwable cause) {
        super(message, cause);
    }

    public CommandException(Throwable cause) {
        super(cause);
    }

    public CommandException(String message,
                            Throwable cause,
                            boolean enableSuppression,
                            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
