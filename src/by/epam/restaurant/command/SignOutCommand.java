package by.epam.restaurant.command;

import by.epam.restaurant.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Enumeration;
import java.util.Optional;

/**
 * Command that deletes user from session
 */
class SignOutCommand implements Command {
    private static final String DEFAULT_URL = "/controller?command=empty";
    private static final String REQUEST_PARAM_USER = "user";
    private static final Logger LOG = LogManager.getLogger(SignOutCommand.class);

    /**
     * Deletes user from session
     * @param request  request from client
     * @param response HttpServletResponse which may be used
     *                 for writing error messages as response to AJAX query
     * @return next page's URL
     * @throws CommandException
     */
    @Override
    public Optional<String> execute(HttpServletRequest request,
                                    HttpServletResponse response)
            throws CommandException {
        HttpSession session = request.getSession(true);
        User user = (User) session.getAttribute(REQUEST_PARAM_USER);
        Enumeration<String> e = session.getAttributeNames();
        while (e.hasMoreElements()) {
            session.removeAttribute(e.nextElement());
        }
        LOG.info("User " + (user != null ? user.getLogin() : "") +
                                                    " has signed out.");
        return Optional.of(DEFAULT_URL);
    }
}