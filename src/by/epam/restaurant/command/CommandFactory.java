package by.epam.restaurant.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Creates new {@link Command} object depending on it's string value.
 * <p>
 * Uses {@link CommandEnum command enumeration} for defining appropriate command.
 * </p>
 */
public class CommandFactory {

    private static final Logger LOG = LogManager.getLogger(CommandFactory.class);
    private static final String REQUEST_PARAM_COMMAND = "command";

    private CommandFactory() {
    }


    /**
     * Tries to define command by given string value.
     * <p>
     * Returns {@link Command} object according to {@code "command"} {@code parameter}
     * value from client's {@code request}.
     * <br/>
     * If {@code parameter} value is {@code null} it returns {@link EmptyCommand} object.
     * </p>
     *
     * @param request request from client that contains parameter with name {@code "command"}
     * @return {@link Command} object from {@link CommandEnum}
     * @throws CommandException if incorrect request {@code "command"} parameter value is given
     * @see CommandEnum
     */
    public static Command defineCommand(HttpServletRequest request)
            throws CommandException {
        String command = request.getParameter(REQUEST_PARAM_COMMAND);
        if (command != null) {
            try {
                LOG.info("Command: " + command);
                return CommandEnum.valueOf(command.toUpperCase()).getCommand();
            } catch (IllegalArgumentException e) {
                throw new CommandException("Wrong command name", e);
            }
        } else {
            LOG.info("Null command");
            return CommandEnum.EMPTY.getCommand();
        }
    }
}