package by.epam.restaurant.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Command that changes user's language in session
 */
class ChangeLanguageCommand implements Command {
    private static final String REGEX_PATTERN = "(^http://.+?)(?=/)";
    private static final String LANG_SESSION_ATTRIBUTE = "lang";
    private static final String LANG_REQUEST_PARAMETER = "lang";
    private static final String REFERER_REQUEST_HEADER = "referer";
    private static final String URL_DEFAULT = "/controller?command=empty";

    /**
     * Changes user's language in session
     * @param request  request from client
     * @param response HttpServletResponse which may be used
     *                 for writing error messages as response to AJAX query
     * @return next page's URL
     * @throws CommandException
     */
    @Override
    public Optional<String> execute(HttpServletRequest request,
                                    HttpServletResponse response)
            throws CommandException {
        String requestLang = request.getParameter(LANG_REQUEST_PARAMETER);
        request.getSession(true).setAttribute(LANG_SESSION_ATTRIBUTE, requestLang);
        String url = request.getHeader(REFERER_REQUEST_HEADER);
        if (url == null) {
            return Optional.of(URL_DEFAULT);
        }
        Matcher prefixMatcher = Pattern.compile(REGEX_PATTERN).matcher(url);
        if (prefixMatcher.find()) {
            url = url.substring(prefixMatcher.end());
        }
        return Optional.of(url);
    }

    @Override
    public boolean needsRedirect() {
        return true;
    }
}
