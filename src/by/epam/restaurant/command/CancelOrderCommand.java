package by.epam.restaurant.command;

import by.epam.restaurant.entity.User;
import by.epam.restaurant.entity.UserRole;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Optional;

/**
 * Command that deletes order from session
 */
class CancelOrderCommand implements Command {
    private static final String SESSION_ATTR_USER = "user";
    private static final String SESSION_ATTR_ORDERED = "ordered";
    private static final String URL_DEFAULT = "/controller?command=empty";
    private static final String URL_MENU = "/controller?command=showmenu";

    /**
     * Deletes order from session
     * @param request  request from client
     * @param response HttpServletResponse which may be used
     *                 for writing error messages as response to AJAX query
     * @return next page's URL
     * @throws CommandException
     */
    @Override
    public Optional<String> execute(HttpServletRequest request,
                                    HttpServletResponse response)
            throws CommandException {
        HttpSession session = request.getSession(true);
        User user = (User) session.getAttribute(SESSION_ATTR_USER);
        if (user == null || user.getRole() != UserRole.CLIENT) {
            return Optional.of(URL_DEFAULT);
        }
        session.removeAttribute(SESSION_ATTR_ORDERED);
        return Optional.of(URL_MENU);
    }
}
