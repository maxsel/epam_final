package by.epam.restaurant.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * This {@code enumeration} represents list of commands that can be executed
 * in this application context.
 * </p>
 * <p>Selecting each item of this {@code enum}
 * creates a new appropriate {@link Command} object
 * that can be returned by calling {@link CommandEnum#getCommand()} method.
 * Than you can call {@link Command#execute(HttpServletRequest, HttpServletResponse)} method
 * on this object to perform defined command.
 * </p>
 * <p>
 * For each element of this {@code enum} see appropriate {@link Command} object docs.
 * </p>
 */
enum CommandEnum {

    /**
     * @see EmptyCommand
     */
    EMPTY {
        {
            command = new EmptyCommand();
        }
    },

    /**
     * @see LogInCommand
     */
    LOGIN {
        {
            command = new LogInCommand();
        }
    },

    /**
     * @see SignUpCommand
     */
    SIGNUP {
        {
            command = new SignUpCommand();
        }
    },

    /**
     * @see ChangeLanguageCommand
     */
    CHANGELANGUAGE {
        {
            command = new ChangeLanguageCommand();
        }
    },

    /**
     * @see ShowMenuCommand
     */
    SHOWMENU {
        {
            command = new ShowMenuCommand();
        }
    },

    /**
     * @see AddDishCommand
     */
    ADDDISH {
        {
            command = new AddDishCommand();
        }
    },

    /**
     * @see EditDishCommand
     */
    EDITDISH {
        {
            command = new EditDishCommand();
        }
    },

    /**
     * @see SaveDishCommand
     */
    SAVEDISH {
        {
            command = new SaveDishCommand();
        }
    },

    /**
     * @see PreOrderCommand
     */
    PREORDER {
        {
            command = new PreOrderCommand();
        }
    },

    /**
     * @see OrderCommand
     */
    ORDER {
        {
            command = new OrderCommand();
        }
    },

    /**
     * @see ShowNewOrdersCommand
     */
    SHOWNEWORDERS {
        {
            command = new ShowNewOrdersCommand();
        }
    },

    /**
     * @see ConfirmOrderCommand
     */
    CONFIRMORDER {
        {
            command = new ConfirmOrderCommand();
        }
    },

    /**
     * @see AskForBillCommand
     */
    ASKFORBILL {
        {
            command = new AskForBillCommand();
        }
    },

    /**
     * @see ShowNewBillsCommand
     */
    SHOWNEWBILLS {
        {
            command = new ShowNewBillsCommand();
        }
    },

    /**
     * @see ConfirmBillCommand
     */
    CONFIRMBILL {
        {
            command = new ConfirmBillCommand();
        }
    },

    /**
     * @see PayBillCommand
     */
    PAYBILL {
        {
            command = new PayBillCommand();
        }
    },

    /**
     * @see ShowBillPaidCommand
     */
    SHOWBILLPAID {
        {
            command = new ShowBillPaidCommand();
        }
    },

    /**
     * @see ShowProfileCommand
     */
    SHOWPROFILE {
        {
            command = new ShowProfileCommand();
        }
    },

    /**
     * @see ShowLoginCommand
     */
    SHOWLOGINPAGE {
        {
            command = new ShowLoginCommand();
        }
    },

    /**
     * @see ShowSignUpPageCommand
     */
    SHOWSIGNUPPAGE {
        {
            command = new ShowSignUpPageCommand();
        }
    },

    /**
     * @see ShowErrorPageCommand
     */
    SHOWERRORPAGE {
        {
            command = new ShowErrorPageCommand();
        }
    },

    /**
     * @see ShowOrderCommand
     */
    SHOWORDER {
        {
            command = new ShowOrderCommand();
        }
    },

    /**
     * @see ShowAboutUsCommand
     */
    SHOWABOUTUS {
        {
            command = new ShowAboutUsCommand();
        }
    },

    /**
     * @see CancelOrderCommand
     */
    CANCELORDER {
        {
            command = new CancelOrderCommand();
        }
    },

    /**
     * @see DeleteOrderCommand
     */
    DELETEORDER {
        {
            command = new DeleteOrderCommand();
        }
    },

    /**
     * @see DeleteDishCommand
     */
    DELETEDISH {
        {
            command = new DeleteDishCommand();
        }
    },

    /**
     * @see RestoreDishCommand
     */
    RESTOREDISH {
        {
            command = new RestoreDishCommand();
        }
    },

    /**
     * @see AddMoneyCommand
     */
    MOREMONEY {
        {
            command = new AddMoneyCommand();
        }
    },

    /**
     * @see SignOutCommand
     */
    SIGNOUT {
        {
            command = new SignOutCommand();
        }
    };

    protected Command command;

    /**
     * <p>
     * Get the current command. Is called by appropriate {@code enum} element.
     * E.g., if you need login command, you're doing:
     * </p>
     * <p>
     * <code>
     * CommandEnum definedCommand = CommandEnum.LOGIN;
     * <br/>
     * String responseAddress = definedCommand.getCommand().execute(request);
     * </code>
     * </p>
     *
     * @return current {@link Command} object.
     */
    public Command getCommand() {
        return command;
    }
}
