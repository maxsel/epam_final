package by.epam.restaurant.command;

import by.epam.restaurant.entity.User;
import by.epam.restaurant.service.ServiceException;
import by.epam.restaurant.service.UserService;
import by.epam.restaurant.util.MD5Digest;
import by.epam.restaurant.validator.UserValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Optional;

/**
 * Command that creates new user
 */
class SignUpCommand implements Command {
    private static final Logger LOG = LogManager.getLogger(SignUpCommand.class);
    private static final String REQUEST_PARAM_LOGIN = "input_login";
    private static final String REQUEST_PARAM_PASSWORD = "input_password";
    private static final String REQUEST_PARAM_REP_PASSWORD = "repeat_password";
    private static final String REQUEST_PARAM_FIRST_NAME = "input_first_name";
    private static final String REQUEST_PARAM_LAST_NAME = "input_last_name";
    private static final String SESSION_ATTR_USER = "user";
    private static final String MESSAGE_PASSWORDS_NOT_MATCH = "passwordsNotMatch";
    private static final String MESSAGE_LOGIN_NOT_FREE = "loginNotFree";
    private static final String MESSAGE_INVALID_LOGIN = "invalidLogin";
    private static final String MESSAGE_INVALID_PASSWORD = "invalidPassword";
    private static final String MESSAGE_EMPTY_FIELD = "emptyField";
    private static final String URL_DEFAULT = "/controller?command=empty";
    private static final String URL_PROFILE = "/controller?command=showprofile";

    /**
     * Creates new user
     * @param request  request from client
     * @param response HttpServletResponse which may be used
     *                 for writing error messages as response to AJAX query
     * @return next page's URL
     * @throws CommandException
     */
    @Override
    public Optional<String> execute(HttpServletRequest request,
                                    HttpServletResponse response)
            throws CommandException {
        HttpSession session = request.getSession(true);
        User curUser = (User) session.getAttribute(SESSION_ATTR_USER);
        if (curUser != null) {
            return Optional.of(URL_DEFAULT);
        }
        String login = request.getParameter(REQUEST_PARAM_LOGIN);
        String password = request.getParameter(REQUEST_PARAM_PASSWORD);
        String repeatPassword = request.getParameter(REQUEST_PARAM_REP_PASSWORD);
        String firstName = request.getParameter(REQUEST_PARAM_FIRST_NAME);
        String lastName = request.getParameter(REQUEST_PARAM_LAST_NAME);
        try {
            if (login == null || password == null || repeatPassword == null
                    || firstName == null || lastName == null) {
                response.getWriter().write(MESSAGE_EMPTY_FIELD);
                return Optional.empty();
            }
            if (!password.equals(repeatPassword)) {
                response.getWriter().write(MESSAGE_PASSWORDS_NOT_MATCH);
                return Optional.empty();
            }
            if (!UserValidator.validLogin(login)) {
                response.getWriter().write(MESSAGE_INVALID_LOGIN);
                return Optional.empty();
            }
            if (!UserValidator.validPassword(password)) {
                response.getWriter().write(MESSAGE_INVALID_PASSWORD);
                return Optional.empty();
            }
            if (UserService.isLoginFree(login)) {
                User user = new User();
                user.setLogin(login);
                user.setPassword(MD5Digest.encrypt(password));
                user.setFirstName(firstName);
                user.setLastName(lastName);
                user = UserService.registerNewUser(user);
                session.setAttribute(SESSION_ATTR_USER, user);
                LOG.info("User " + user.getLogin() + " has signed up");
                response.getWriter().write(URL_PROFILE);
            } else {
                response.getWriter().write(MESSAGE_LOGIN_NOT_FREE);
            }
        } catch (ServiceException | IOException e) {
            throw new CommandException(e);
        }
        return Optional.empty();
    }
}
