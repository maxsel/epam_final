package by.epam.restaurant.command;

import by.epam.restaurant.entity.Category;
import by.epam.restaurant.entity.User;
import by.epam.restaurant.entity.UserRole;
import by.epam.restaurant.service.CategoryService;
import by.epam.restaurant.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;

/**
 * Command that adds new dish to database
 */
class AddDishCommand implements Command {
    private static final String SESSION_ATTR_CATEGORIES = "categories";
    private static final String SESSION_ATTR_DISH = "dish";
    private static final String SESSION_ATTR_USER = "user";
    private static final String URL_DEFAULT = "/controller?command=empty";
    private static final String URL_EDIT_DISH = "/jsp/edit_dish.jsp";

    /**
     * Adds new dish to database
     * @param request  request from client
     * @param response HttpServletResponse which may be used
     *                 for writing error messages as response to AJAX query
     * @return next page's URL
     * @throws CommandException
     */
    @Override
    public Optional<String> execute(HttpServletRequest request,
                                    HttpServletResponse response)
            throws CommandException {
        try {
            HttpSession session = request.getSession(true);
            User user = (User) session.getAttribute(SESSION_ATTR_USER);
            if (user == null || user.getRole() != UserRole.ADMINISTRATOR) {
                return Optional.of(URL_DEFAULT);
            }
            List<Category> categories = CategoryService.findAll();
            session.setAttribute(SESSION_ATTR_CATEGORIES, categories);
            session.removeAttribute(SESSION_ATTR_DISH);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return Optional.of(URL_EDIT_DISH);
    }
}
