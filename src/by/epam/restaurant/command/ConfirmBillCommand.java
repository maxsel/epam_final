package by.epam.restaurant.command;

import by.epam.restaurant.entity.User;
import by.epam.restaurant.entity.UserRole;
import by.epam.restaurant.service.BillService;
import by.epam.restaurant.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * Command that confirms bill from admin account
 */
class ConfirmBillCommand implements Command {
    private static final String REQUEST_PARAM_BILL_TO_CONFIRM = "billIdToConfirm";
    private static final String SESSION_ATTR_USER = "user";
    private static final String URL_DEFAULT = "/controller?command=empty";
    private static final String URL_SHOW_NEW_BILLS = "/controller?command=shownewbills";

    /**
     * Confirms bill from admin account
     * @param request  request from client
     * @param response HttpServletResponse which may be used
     *                 for writing error messages as response to AJAX query
     * @return next page's URL
     * @throws CommandException
     */
    @Override
    public Optional<String> execute(HttpServletRequest request,
                                    HttpServletResponse response)
            throws CommandException {
        try {
            User user = (User) request.getSession(true).getAttribute(SESSION_ATTR_USER);
            if (user == null || user.getRole() != UserRole.ADMINISTRATOR) {
                return Optional.of(URL_DEFAULT);
            }
            long id = Long.valueOf(request.getParameter(REQUEST_PARAM_BILL_TO_CONFIRM));
            LocalDateTime confirmDateTime = LocalDateTime.now();
            BillService.confirmById(id, confirmDateTime);
            return Optional.of(URL_SHOW_NEW_BILLS);
        } catch (ServiceException | NumberFormatException e) {
            throw new CommandException(e);
        }
    }

    @Override
    public boolean needsRedirect() {
        return true;
    }
}
