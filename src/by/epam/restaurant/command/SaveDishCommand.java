package by.epam.restaurant.command;

import by.epam.restaurant.entity.Dish;
import by.epam.restaurant.entity.ItemStatus;
import by.epam.restaurant.entity.User;
import by.epam.restaurant.entity.UserRole;
import by.epam.restaurant.service.CategoryService;
import by.epam.restaurant.service.DishService;
import by.epam.restaurant.service.ServiceException;
import by.epam.restaurant.validator.DishValidator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

/**
 * Command that saves edited dish from admin account
 */
class SaveDishCommand implements Command {
    private static final String REQUEST_PARAM_INPUT_NAME = "input_name";
    private static final String REQUEST_PARAM_INPUT_DESCRIPTION = "input_description";
    private static final String REQUEST_PARAM_INPUT_PRICE = "input_price";
    private static final String REQUEST_PARAM_INPUT_STATUS = "input_status";
    private static final String REQUEST_PARAM_INPUT_CATEGORY = "input_category";
    private static final String REQUEST_PARAM_INPUT_SIZE = "input_size";
    private static final String REQUEST_PARAM_DISH_ID = "dish_id";
    private static final String SESSION_ATTR_USER = "user";
    private static final String URL_DEFAULT = "/controller?command=empty";
    private static final String URL_SHOW_MENU = "/controller?command=showmenu";
    private static final String MESSAGE_FAIL = "fail";

    /**
     * Saves edited dish from admin account
     * @param request  request from client
     * @param response HttpServletResponse which may be used
     *                 for writing error messages as response to AJAX query
     * @return next page's URL
     * @throws CommandException
     */
    @Override
    public Optional<String> execute(HttpServletRequest request,
                                    HttpServletResponse response)
            throws CommandException {
        User user = (User) request.getSession(true).getAttribute(SESSION_ATTR_USER);
        if (user == null || user.getRole() != UserRole.ADMINISTRATOR) {
            return Optional.of(URL_DEFAULT);
        }
        String name = request.getParameter(REQUEST_PARAM_INPUT_NAME);
        String description = request.getParameter(REQUEST_PARAM_INPUT_DESCRIPTION);
        String price = request.getParameter(REQUEST_PARAM_INPUT_PRICE);
        String status = request.getParameter(REQUEST_PARAM_INPUT_STATUS);
        String category = request.getParameter(REQUEST_PARAM_INPUT_CATEGORY);
        String size = request.getParameter(REQUEST_PARAM_INPUT_SIZE);
        String id = request.getParameter(REQUEST_PARAM_DISH_ID);

        try {
            if (!DishValidator.isValid(name, price, status, category, size)) {
                response.getWriter().write(MESSAGE_FAIL);
                return Optional.empty();
            }
            Dish dish = new Dish(0, name, Long.parseLong(price), description,
                                ItemStatus.fromLong(Long.parseLong(status)),
                                CategoryService.findById(Long.parseLong(category)),
                                Long.parseLong(size));
            if (id == null || id.isEmpty()) {
                DishService.create(dish);
            } else {
                dish.setId(Long.parseLong(id));
                DishService.update(dish);
            }
            response.getWriter().write(URL_SHOW_MENU);
            return Optional.empty();
        } catch (ServiceException | IOException e) {
            throw new CommandException(e);
        }
    }
}