package by.epam.restaurant.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

/**
 * Command that shows error page
 */
class ShowErrorPageCommand implements Command {
    private static final String URL_ERROR = "/jsp/error.jsp";

    /**
     * Shows error page
     * @param request  request from client
     * @param response HttpServletResponse which may be used
     *                 for writing error messages as response to AJAX query
     * @return next page's URL
     * @throws CommandException
     */
    @Override
    public Optional<String> execute(HttpServletRequest request,
                                    HttpServletResponse response)
            throws CommandException {
        return Optional.of(URL_ERROR);
    }
}
