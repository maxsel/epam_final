package by.epam.restaurant.command;

import by.epam.restaurant.entity.*;
import by.epam.restaurant.service.OrderService;
import by.epam.restaurant.service.OrderedDishService;
import by.epam.restaurant.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Command that shows new orders
 */
class ShowNewOrdersCommand implements Command {
    private static final String SESSION_ATTR_USER = "user";
    private static final String REQUEST_PARAM_ORDERS = "orders";
    private static final String REQUEST_PARAM_CONFIRMED = "confirmed";
    private static final String URL_DEFAULT = "/controller?command=empty";
    private static final String URL_NEW_ORDERS = "/jsp/new_orders.jsp";

    /**
     * Shows new orders
     * @param request  request from client
     * @param response HttpServletResponse which may be used
     *                 for writing error messages as response to AJAX query
     * @return next page's URL
     * @throws CommandException
     */
    @Override
    public Optional<String> execute(HttpServletRequest request,
                                    HttpServletResponse response)
            throws CommandException {
        try {
            User user =
                    (User) request.getSession(true).getAttribute(SESSION_ATTR_USER);
            if (user == null) {
                return Optional.of(URL_DEFAULT);
            }
            List<Order> allOrders;
            List<Order> unpaidOrders = new ArrayList<>();
            List<Order> confirmedOrders = new ArrayList<>();
            if (user.getRole() == UserRole.CLIENT) {
                allOrders = OrderService.findAllOfUser(user);
                unpaidOrders = allOrders
                        .stream()
                        .filter(order -> order.getOrderStatus() != OrderStatus.PAID)
                        .collect(Collectors.toList());
                confirmedOrders = allOrders
                        .stream()
                        .filter(order -> order.getOrderStatus() == OrderStatus.CONFIRMED)
                        .collect(Collectors.toList());
            }
            if (user.getRole() == UserRole.ADMINISTRATOR) {
                unpaidOrders = OrderService.findAll();
            }
            Map<Order, List<OrderedDish>> mapOrders = new HashMap<>();
            for (Order order : unpaidOrders) {
                mapOrders.put(order, OrderedDishService.findAllOfOrder(order));
            }
            request.setAttribute(REQUEST_PARAM_ORDERS, mapOrders);
            request.setAttribute(REQUEST_PARAM_CONFIRMED, confirmedOrders);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return Optional.of(URL_NEW_ORDERS);
    }
}
