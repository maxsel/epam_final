package by.epam.restaurant.command;

import by.epam.restaurant.entity.User;
import by.epam.restaurant.service.ServiceException;
import by.epam.restaurant.service.UserService;
import by.epam.restaurant.util.MD5Digest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Optional;

/**
 * Command that logs user in
 */
class LogInCommand implements Command {
    private static final String REQUEST_PARAM_LOGIN = "input_login";
    private static final String REQUEST_PARAM_PASSWORD = "input_password";
    private static final String SESSION_ATTR_USER = "user";
    private static final String MESSAGE_FAIL = "fail";
    private static final String URL_DEFAULT = "/controller?command=empty";
    private static final String URL_PROFILE = "/controller?command=showprofile";
    private static final Logger LOG = LogManager.getLogger(LogInCommand.class);

    /**
     * Logs user in
     * @param request  request from client
     * @param response HttpServletResponse which may be used
     *                 for writing error messages as response to AJAX query
     * @return next page's URL
     * @throws CommandException
     */
    @Override
    public Optional<String> execute(HttpServletRequest request,
                                    HttpServletResponse response)
            throws CommandException {
        HttpSession session = request.getSession(true);
        User curUser = (User) session.getAttribute(SESSION_ATTR_USER);
        if (curUser != null) {
            return Optional.of(URL_DEFAULT);
        }
        String login = request.getParameter(REQUEST_PARAM_LOGIN);
        String password = request.getParameter(REQUEST_PARAM_PASSWORD);
        try {
            User user =
                    UserService.findByCredentials(login, MD5Digest.encrypt(password));
            if (user != null) {
                session.setAttribute(SESSION_ATTR_USER, user);
                LOG.info("User " + user.getLogin() + " has logged in");
                response.getWriter().write(URL_PROFILE);
            } else {
                response.getWriter().write(MESSAGE_FAIL);
            }
        } catch (ServiceException | IOException e) {
            throw new CommandException("Error while logging in", e);
        }
        return Optional.empty();
    }
}