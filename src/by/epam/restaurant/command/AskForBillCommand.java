package by.epam.restaurant.command;

import by.epam.restaurant.entity.*;
import by.epam.restaurant.service.BillService;
import by.epam.restaurant.service.OrderService;
import by.epam.restaurant.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Command that creates new bill when user asks
 */
class AskForBillCommand implements Command {
    private static final String SESSION_ATTR_USER = "user";
    private static final String URL_DEFAULT = "/controller?command=empty";
    private static final String URL_SHOW_NEW_BILLS = "/controller?command=shownewbills";

    /** Creates new bill when user asks
     * @param request  request from client
     * @param response HttpServletResponse which may be used
     *                 for writing error messages as response to AJAX query
     * @return next page's URL
     * @throws CommandException
     */
    @Override
    public Optional<String> execute(HttpServletRequest request,
                                    HttpServletResponse response)
            throws CommandException {
        try {
            User user = (User) request.getSession(true).getAttribute(SESSION_ATTR_USER);
            if (user == null || user.getRole() != UserRole.CLIENT) {
                return Optional.of(URL_DEFAULT);
            }
            Bill bill = new Bill();
            bill.setStatus(BillStatus.NEW);
            bill.setClient(user);
            bill.setCreationDate(LocalDateTime.now());
            bill = BillService.create(bill);
            List<Order> confirmedOrders = OrderService.findAllConfirmedOfUser(user);
            for (Order order : confirmedOrders) {
                order.setBill(bill);
                order.setOrderStatus(OrderStatus.PAID);
                OrderService.update(order);
            }
            response.getWriter().write(URL_SHOW_NEW_BILLS);
        } catch (ServiceException | IOException e) {
            throw new CommandException(e);
        }
        return Optional.empty();
    }
}
