package by.epam.restaurant.command;

import by.epam.restaurant.entity.*;
import by.epam.restaurant.service.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Command that pays bill from user account
 */
class PayBillCommand implements Command {
    private static final String SESSION_ATTR_USER = "user";
    private static final String REQUEST_PARAM_BILL_ID_TO_PAY = "billIdToPay";
    private static final String URL_DEFAULT = "/controller?command=empty";
    private static final String URL_BILL_PAID = "/controller?command=showbillpaid";
    private static final String MESSAGE_FAIL = "fail";

    /**
     * Pays bill from user account
     * @param request  request from client
     * @param response HttpServletResponse which may be used
     *                 for writing error messages as response to AJAX query
     * @return next page's URL
     * @throws CommandException
     */
    @Override
    public Optional<String> execute(HttpServletRequest request,
                                    HttpServletResponse response)
            throws CommandException {
        try {
            HttpSession session = request.getSession(true);
            User user = (User) session.getAttribute(SESSION_ATTR_USER);
            if (user == null || user.getRole() != UserRole.CLIENT) {
                response.getWriter().write(URL_DEFAULT);
                return Optional.empty();
            }
            long id = Long.valueOf(request.getParameter(REQUEST_PARAM_BILL_ID_TO_PAY));
            Bill bill = BillService.findById(id);
            List<OrderedDish> dishes = new ArrayList<>();
            for (Order order : OrderService.findAllOfBill(bill)) {
                dishes.addAll(OrderedDishService.findAllOfOrder(order));
            }
            long cost = dishes.stream().mapToLong(OrderedDish::getInstantBill).sum();
            if (cost <= user.getBalance()) {
                BillService.payById(id, LocalDateTime.now());
                user.setBalance(user.getBalance() - cost);
                user = UserService.update(user);
                session.setAttribute(SESSION_ATTR_USER, user);
                response.getWriter().write(URL_BILL_PAID);
            } else {
                response.getWriter().write(MESSAGE_FAIL);
            }
            return Optional.empty();
        } catch (ServiceException | IOException | NumberFormatException e) {
            throw new CommandException(e);
        }
    }

    @Override
    public boolean needsRedirect() {
        return true;
    }
}
