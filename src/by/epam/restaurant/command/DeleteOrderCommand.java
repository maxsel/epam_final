package by.epam.restaurant.command;

import by.epam.restaurant.entity.*;
import by.epam.restaurant.service.OrderService;
import by.epam.restaurant.service.OrderedDishService;
import by.epam.restaurant.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * Command that deletes unconfirmed order from client's account
 */
class DeleteOrderCommand implements Command {
    private static final String REQUEST_PARAM_ORDER_TO_DELETE = "orderId";
    private static final String SESSION_ATTR_USER = "user";
    private static final String URL_DEFAULT = "/controller?command=empty";
    private static final String URL_SHOW_NEW_ORDERS = "/controller?command=showneworders";

    /**
     * Deletes unconfirmed order from client's account
     * @param request  request from client
     * @param response HttpServletResponse which may be used
     *                 for writing error messages as response to AJAX query
     * @return next page's URL
     * @throws CommandException
     */
    @Override
    public Optional<String> execute(HttpServletRequest request,
                                    HttpServletResponse response)
            throws CommandException {
        try {
            User user =
                    (User) request.getSession(true).getAttribute(SESSION_ATTR_USER);
            if (user == null || user.getRole() != UserRole.CLIENT) {
                return Optional.of(URL_DEFAULT);
            }
            long id =
                    Long.valueOf(request.getParameter(REQUEST_PARAM_ORDER_TO_DELETE));
            Order order = OrderService.findById(id);
            if (order != null && order.getOrderStatus() == OrderStatus.NEW) {
                OrderedDishService.deleteAllOfOrder(order);
                OrderService.delete(order);
            }
            return Optional.of(URL_SHOW_NEW_ORDERS);
        } catch (ServiceException | NumberFormatException e) {
            throw new CommandException(e);
        }
    }

    @Override
    public boolean needsRedirect() {
        return true;
    }
}