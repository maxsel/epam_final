package test.by.epam.restaurant;

import by.epam.restaurant.validator.UserValidator;

import static org.junit.Assert.*;

public class UserValidatorTest {
    @org.junit.Test
    public void validLogin1() throws Exception {
        assertFalse("login too short", UserValidator.validLogin("hi"));
    }

    @org.junit.Test
    public void validLogin2() throws Exception {
        assertFalse("incorrect chars in login",
                UserValidator.validLogin("cr#zy_^ser"));
    }

    @org.junit.Test
    public void validLogin3() throws Exception {
        assertTrue("valid login", UserValidator.validLogin("maxsel"));
    }

    @org.junit.Test
    public void validPassword1() throws Exception {
        assertFalse("password too short", UserValidator.validPassword("short"));
    }

    @org.junit.Test
    public void validPassword2() throws Exception {
        assertFalse("valid password", UserValidator.validPassword("short"));
    }
}