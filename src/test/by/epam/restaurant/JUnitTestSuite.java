package test.by.epam.restaurant;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
@RunWith(Suite.class)
@Suite.SuiteClasses({
        UserValidatorTest.class,
        UserServiceTest.class,
        ResourceManagerTest.class
})
public class JUnitTestSuite {
}