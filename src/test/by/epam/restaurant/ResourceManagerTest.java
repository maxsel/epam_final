package test.by.epam.restaurant;

import by.epam.restaurant.util.ResourceManager;
import org.junit.Before;
import org.junit.Test;

import java.util.Locale;

import static org.junit.Assert.*;

public class ResourceManagerTest {
    private static final String STRING_EN = "Ask for bill";
    private static final String STRING_BE = "Папрасіць рахунак";
    private static final String STRING_RU = "Попросить счёт";
    private static final String ASK_BILL = "ask.bill";
    private static final String NOT_PRESENT = "any.not.present.message";
    private ResourceManager manager = ResourceManager.INSTANCE;

    @Before
    public void setUp() throws Exception {
        manager.changeResource(new Locale("en", "US"));
    }

    @Test
    public void getString1() throws Exception {
        assertTrue(STRING_EN.equals(manager.getString(ASK_BILL)));
    }

    @Test
    public void getString2() throws Exception {
        manager.changeResource(new Locale("be", "BY"));
        assertTrue(STRING_BE.equals(manager.getString(ASK_BILL)));
    }

    @Test
    public void getString3() throws Exception {
        manager.changeResource(new Locale("ru", "RU"));
        assertTrue(STRING_RU.equals(manager.getString(ASK_BILL)));
    }

    @Test(expected = java.util.MissingResourceException.class)
    public void getString4() throws Exception {
        manager.getString(NOT_PRESENT);
    }
}