package test.by.epam.restaurant;

import by.epam.restaurant.entity.User;
import by.epam.restaurant.service.UserService;
import by.epam.restaurant.util.MD5Digest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserServiceTest {
    private static final String LOGIN = "newser";
    private static final String PASSWORD = MD5Digest.encrypt("password");
    private static final String FIRST_NAME = "Eric";
    private static final String LAST_NAME = "Ericsson";
    private static final String FAKE_PASSWORD = "not_a_passwd_at_all";
    private static final String NEW_LOGIN = "BRAND_NEW_LOGIN";
    private User user = new User();

    @Before
    public void setUp() throws Exception {
        user.setLogin(LOGIN);
        user.setPassword(PASSWORD);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        UserService.registerNewUser(user);
    }

    @After
    public void tearDown() throws Exception {
        UserService.delete(user);
    }

    @Test
    public void findByCredentials1() throws Exception {
        assertTrue("present user",
                UserService.findByCredentials(LOGIN, PASSWORD).getId() == user.getId());
    }

    @Test
    public void findByCredentials2() throws Exception {
        assertNull("not present user",
                UserService.findByCredentials(LOGIN, FAKE_PASSWORD));
    }

    @Test
    public void isLoginFree1() throws Exception {
        assertTrue("free login",
                UserService.isLoginFree(NEW_LOGIN));
    }

    @Test
    public void isLoginFree2() throws Exception {
        assertFalse("occupied login",
                UserService.isLoginFree(user.getLogin()));
    }

}